package com.homeshoppinglistApp.presentation.beans;

import com.homeshoppinglistApp.business.Home;
import com.homeshoppinglistApp.business.HomeRoom;
import com.homeshoppinglistApp.business.Membre;
import com.homeshoppinglistApp.business.UserStatusEnum;
import com.homeshoppinglistApp.services.Service;
import com.homeshoppinglistApp.utilities.SessionUtils;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.enterprise.context.RequestScoped;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.servlet.http.HttpSession;
import org.primefaces.context.RequestContext;

/**
 *
 * @author BorisK<klettboris@gmail.com>
 */
@ManagedBean(name = "homeBean")
@RequestScoped
public class HomeBean implements Serializable {

    private static final long serialVersionUID = 1094801825228386363L;

    @Inject
    private Service services;

    private String homeName;
    private String homeStreet;
    private String homeZipcode;
    private String homeCity;
    private List<Membre> members = new ArrayList<>();
    private Home home;
    private Membre member;
    private String userBirthdateToString;
    private String userLastname;
    private String userFirstname;
    private UserStatusEnum status;
    private String userEmail;
    private String statusCode;
    private boolean isDisabled = false;
    private List<UserStatusEnum> statusList = new ArrayList<>();
    private HttpSession session;
    private String delHomeConfMess;
    private boolean homeButtonRendered = true;
    private boolean homeDetailDisabled = false;
    private boolean otherButtonRendered = true;

    public HomeBean() {
    }

    @PostConstruct
    public void init() {
        this.postInit();

    }

    public void postInit() {
        FacesMessage msg;
        try {
            session = SessionUtils.getSession();
            this.home = new Home();
            this.home = this.services.getHomeByNameService((String) session.getAttribute("homeName"));
        } catch (Exception ex) {
        }
        List<Membre> memList = new ArrayList<>();
        memList.addAll(this.services.getMembersByHomeService(home));

        this.members = new ArrayList<>();
        memList.stream().filter((m) -> (m.getEndDate() == null)).forEachOrdered((m) -> {
            this.members.add(m);
        });

        List<UserStatusEnum> nmsl = new ArrayList<>();
        this.statusList = new ArrayList<>();
        nmsl.addAll(this.services.getStatusList());
        Membre m = this.services.getCurrentMemberShipByEmailNHomeService((String) this.session.getAttribute("userEmail"), this.home.getName());

        nmsl.forEach((us) -> {
            if (us.getCode().equalsIgnoreCase("SupAd") && !m.getStatus().getCode().equalsIgnoreCase("SupAd")) {
                //No thing to do
            } else if (us.getCode().equalsIgnoreCase("SupAd") && m.getStatus().getCode().equalsIgnoreCase("SupAd")) {
                this.statusList.add(us);
            } else {
                this.statusList.add(us);
            }
        });

        this.homeCity = this.home.getCity();
        this.homeName = this.home.getName();
        this.homeStreet = this.home.getStreet();
        this.homeZipcode = this.home.getZipCode();

        List<HomeRoom> homeRoomList = new ArrayList<>();
        homeRoomList.addAll(this.services.getHomeRoomsByHomeService(home));

        if (!homeRoomList.isEmpty() && this.members.size() > 1) {
            this.delHomeConfMess = "Plusieurs pièces et membres font encore partie de ce foyer! Sohaitez vous quand même supprimer le foyer: " + home.getName() + "?";
            msg = new FacesMessage(FacesMessage.SEVERITY_WARN, "", "Plusieurs pièces et membres font encore partie de ce foyer! Sohaitez vous quand même supprimer le foyer: " + home.getName() + "?");
            FacesContext.getCurrentInstance().addMessage("myFormDelHome:delHomeMsg", msg);
        } else if (!homeRoomList.isEmpty()) {
            this.delHomeConfMess = "Plusieurs pièces font encore partie de ce foyer! Sohaitez vous quand même supprimer le foyer: " + home.getName() + "?";
            msg = new FacesMessage(FacesMessage.SEVERITY_WARN, "", "Plusieurs pièces font encore partie de ce foyer! Sohaitez vous quand même supprimer le foyer: " + home.getName() + "?");
            FacesContext.getCurrentInstance().addMessage("myFormDelHome:delHomeMsg", msg);
        } else if (this.members.size() > 1) {
            this.delHomeConfMess = "Plusieurs membres font encore partie de ce foyer! Sohaitez vous quand même supprimer le foyer: " + home.getName() + "?";
            msg = new FacesMessage(FacesMessage.SEVERITY_WARN, "", "Plusieurs membres font encore partie de ce foyer! Sohaitez vous quand même supprimer le foyer: " + home.getName() + "?");
            FacesContext.getCurrentInstance().addMessage("myFormDelHome:delHomeMsg", msg);
        } else {
            this.delHomeConfMess = "Sohaitez vous supprimer le foyer: " + home.getName() + "?";
            msg = new FacesMessage(FacesMessage.SEVERITY_WARN, "", "Sohaitez vous supprimer le foyer: " + home.getName() + "?");
            FacesContext.getCurrentInstance().addMessage("myFormDelHome:delHomeMsg", msg);
        }

        this.isDisabled = false;
        this.homeButtonRendered = true;
        this.homeDetailDisabled = false;
        otherButtonRendered = true;

        Membre m1 = this.services.getCurrentMemberShipByEmailNHomeService((String) this.session.getAttribute("userEmail"), this.home.getName());
        this.isDisabled = false;
        if (m1.getStatus().getCode().equalsIgnoreCase("memb")) {
            this.isDisabled = true;
            this.homeButtonRendered = false;
            this.homeDetailDisabled = true;
            otherButtonRendered = false;
        } else if (m1.getStatus().getCode().equalsIgnoreCase("SupAd")) {
            this.homeButtonRendered = true;
            this.homeDetailDisabled = false;
        } else {
            this.homeButtonRendered = false;
            this.homeDetailDisabled = true;
        }

        this.statusCode = "-- SelectOne --";
    }

    public void updateHome() {

        Boolean isOk = true;
        Home h = this.services.getHomeByNameService(homeName);
        if (h == null) {
            isOk = false;
        }

        if (h != null && !homeName.equalsIgnoreCase(this.home.getName())) {
            FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_ERROR, "", "Ce nom de foyer a déjà été utilisé");
            FacesContext.getCurrentInstance().addMessage("homeDetailForm:homeDetailMsg", msg);
            RequestContext.getCurrentInstance().update("homeDetailForm");
            return;
        }

        h = new Home();
        h = this.services.getHomeByAddressService(homeStreet, this.homeZipcode, homeCity);
        if (h == null || !isOk) {
            isOk = false;
        }

        if (h != null && !h.getName().equalsIgnoreCase(this.home.getName())) {
            FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_ERROR, "", "Cette adresse a déjà été utilisée");
            FacesContext.getCurrentInstance().addMessage("homeDetailForm:homeDetailMsg", msg);
            RequestContext.getCurrentInstance().update("homeDetailForm");
            return;
        }

        if (isOk) {
            return;
        }

        this.home.setCity(this.homeCity);
        this.home.setName(this.homeName);
        this.home.setStreet(this.homeStreet);
        this.home.setZipCode(this.homeZipcode);

        this.services.updateHomeService(this.home);
        this.session.setAttribute("homeName", this.home.getName());
        this.postInit();

        this.homeCity = this.home.getCity();
        this.homeName = this.home.getName();
        this.homeStreet = this.home.getStreet();
        this.homeZipcode = this.home.getZipCode();
        FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_INFO, "", "Vous avez modifié le foyer");
        FacesContext.getCurrentInstance().addMessage("homeDetailForm:homeDetailMsg", msg);
        RequestContext.getCurrentInstance().update("homeDetailForm");
    }

    public void cleanMemb() {
        this.postInit();
        this.statusCode = "-- SelectOne --";
        this.isDisabled = true;
        this.member = null;
        this.session.setAttribute("memberId", null);
    }

    public void initMemb(Integer p_id) {
        this.postInit();
        this.member = this.services.getMemberByIdService(p_id);
        this.session.setAttribute("memberId", p_id);
        Membre m = this.services.getCurrentMemberShipByEmailNHomeService((String) this.session.getAttribute("userEmail"), this.home.getName());
        this.isDisabled = false;
        if (m.getStatus().getCode().equalsIgnoreCase("memb")) {
            this.isDisabled = true;
            this.homeButtonRendered = false;
            this.homeDetailDisabled = true;
            otherButtonRendered = false;
        } else if (m.getStatus().getCode().equalsIgnoreCase("SupAd")) {
            this.homeButtonRendered = true;
            this.homeDetailDisabled = false;
        } else {
            this.homeButtonRendered = false;
            this.homeDetailDisabled = true;
        }

        if (m.getUser().getEmail().equalsIgnoreCase(this.member.getUser().getEmail()) || this.member.getStatus().getCode().equalsIgnoreCase("SupAd")) {
            this.isDisabled = true;
        }
    }

    public void updateMember() {
        if (this.statusCode.equalsIgnoreCase("-- SelectOne --")) {
            return;
        }

        this.member = this.services.getMemberByIdService((Integer) this.session.getAttribute("memberId"));
        this.member.setStatus(this.services.getStatusByCodeService(this.statusCode));
        this.services.updateMemberService(this.member);
        Membre m = this.services.getCurrentMemberShipByEmailNHomeService((String) this.session.getAttribute("userEmail"), this.home.getName());
        this.isDisabled = false;
        if (m.getStatus().getCode().equalsIgnoreCase("memb")) {
            this.isDisabled = true;
            this.homeButtonRendered = false;
            this.homeDetailDisabled = true;
            otherButtonRendered = false;
        } else if (m.getStatus().getCode().equalsIgnoreCase("SupAd")) {
            this.homeButtonRendered = true;
            this.homeDetailDisabled = false;
        } else {
            this.homeButtonRendered = false;
            this.homeDetailDisabled = true;
        }
        this.cleanMemb();
    }

    public void deleteMember() {
        this.member = this.services.getMemberByIdService((Integer) this.session.getAttribute("memberId"));
        this.member.setEndDate(new Date());
        this.services.updateMemberService(this.member);
        Membre m = this.services.getCurrentMemberShipByEmailNHomeService((String) this.session.getAttribute("userEmail"), this.home.getName());
        this.isDisabled = false;
        if (m.getStatus().getCode().equalsIgnoreCase("memb")) {
            this.isDisabled = true;
            this.homeButtonRendered = false;
            this.homeDetailDisabled = true;
            otherButtonRendered = false;
        } else if (m.getStatus().getCode().equalsIgnoreCase("SupAd")) {
            this.homeButtonRendered = true;
            this.homeDetailDisabled = false;
        } else {
            this.homeButtonRendered = false;
            this.homeDetailDisabled = true;
        }

        this.cleanMemb();
    }

    public String deleteHome() {
        this.postInit();
        Home h = this.services.getHomeByNameService(this.homeName);
        services.deleteHomeService(h);
        FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_INFO, "", "Le foyer: " + h.getName() + " a été supprimé.");
        FacesContext.getCurrentInstance().addMessage("homes:homesMsg", msg);
        return "success";
    }

    //--------------------------------------- Getter and setter -------------------------------------------------
    public boolean isOtherButtonRendered() {
        return otherButtonRendered;
    }

    public void setOtherButtonRendered(boolean otherButtonRendered) {
        this.otherButtonRendered = otherButtonRendered;
    }

    public String getUserLastname() {
        try {
            return this.member.getUser().getLastname();
        } catch (NullPointerException ex) {
            return this.userLastname;
        }
    }

    public String getStatusCode() {
        try {
            return this.member.getStatus().getCode();
        } catch (NullPointerException ex) {
            return this.statusCode;
        }
    }

    public void setStatusCode(String statusCode) {
        this.statusCode = statusCode;
    }

    public void setUserLastname(String userLastname) {
        this.userLastname = userLastname;
    }

    public String getUserFirstname() {
        try {
            return this.member.getUser().getFirstname();
        } catch (NullPointerException ex) {
            this.isDisabled = true;
            return this.userFirstname;

        }
    }

    public void setUserFirstname(String userFirstname) {
        this.userFirstname = userFirstname;
    }

    public String getUserBirthdateToString() {
        try {
            this.userBirthdateToString = Service.FORMATTER_FULL_YEAR_FORMAT.format(this.member.getStartDate());
        } catch (NullPointerException ex) {
            return userBirthdateToString;
        }
        return userBirthdateToString;
    }

    public void setUserBirthdateToString(String userBirthdateToString) {
        this.userBirthdateToString = userBirthdateToString;
    }

    public UserStatusEnum getStatus() {
        try {
            this.status = this.member.getStatus();
        } catch (NullPointerException ex) {
            return status;
        }
        return status;
    }

    public void setStatus(UserStatusEnum status) {
        this.status = status;
    }

    public String getUserEmail() {
        try {
            this.userEmail = this.member.getUser().getEmail();
        } catch (NullPointerException ex) {
            return userEmail;
        }
        return userEmail;
    }

    public void setUserEmail(String userEmail) {
        this.userEmail = userEmail;
    }

    public String getHomeName() {
        return homeName;
    }

    public void setHomeName(String homeName) {
        this.homeName = homeName;
    }

    public String getHomeStreet() {
        return homeStreet;
    }

    public void setHomeStreet(String homeStreet) {
        this.homeStreet = homeStreet;
    }

    public String getHomeZipcode() {
        return homeZipcode;
    }

    public void setHomeZipcode(String homeZipcode) {
        this.homeZipcode = homeZipcode;
    }

    public String getHomeCity() {
        return homeCity;
    }

    public void setHomeCity(String homeCity) {
        this.homeCity = homeCity;
    }

    public List<Membre> getMembers() {
        return members;
    }

    public void setMembers(List<Membre> members) {
        this.members = members;
    }

    public Membre getMember() {
        return member;
    }

    public void setMember(Membre member) {
        this.member = member;
    }

    public List<UserStatusEnum> getStatusList() {
        return statusList;
    }

    public void setStatusList(List<UserStatusEnum> statusList) {
        this.statusList = statusList;
    }

    public boolean isIsDisabled() {
        return isDisabled;
    }

    public void setIsDisabled(boolean isDisabled) {
        this.isDisabled = isDisabled;
    }

    public String getDelHomeConfMess() {
        return delHomeConfMess;
    }

    public void setDelHomeConfMess(String delHomeConfMess) {
        this.delHomeConfMess = delHomeConfMess;
    }

    public boolean isHomeButtonRendered() {
        return homeButtonRendered;
    }

    public void setHomeButtonRendered(boolean homeButtonRendered) {
        this.homeButtonRendered = homeButtonRendered;
    }

    public boolean isHomeDetailDisabled() {
        return homeDetailDisabled;
    }

    public void setHomeDetailDisabled(boolean homeDetailDisabled) {
        this.homeDetailDisabled = homeDetailDisabled;
    }

}
