package com.homeshoppinglistApp.presentation.beans;

import java.io.Serializable;
import java.util.Locale;
import javax.enterprise.context.SessionScoped;
import javax.faces.context.FacesContext;
import javax.inject.Named;

/**
 * This backing bean allows to do the internationnalization
 *
 * @author boris.klett <klettboris@gmail.com>
 */
@Named
@SessionScoped
public class LanguageBean implements Serializable {

    private static final long serialVersionUID = 1L;
    private Locale locale;

    /**
     * This is the constructor of the class This constructor set the system
     * language to French
     */
    public LanguageBean() {
        this.locale = new Locale("FR");
    }

    /**
     * This method allow to change the system language
     *
     * @param lang is the wanted language
     * @return success
     */
    public String setLocale(String lang) {
        locale = new Locale(lang);
        FacesContext.getCurrentInstance().getViewRoot().setLocale(locale);
        return "success";
    }

    /**
     * This method allow to get the system language
     *
     * @return the system language
     */
    public Locale getLocale() {
        //return locale.toString();
        return this.locale;
    }
}
