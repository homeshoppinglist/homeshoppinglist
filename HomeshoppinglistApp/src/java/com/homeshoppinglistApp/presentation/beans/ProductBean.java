package com.homeshoppinglistApp.presentation.beans;

import com.homeshoppinglistApp.business.HomeRoom;
import com.homeshoppinglistApp.business.HomeRoomProduct;
import com.homeshoppinglistApp.business.Product;
import com.homeshoppinglistApp.business.UnityQuantityEnum;
import com.homeshoppinglistApp.services.Service;
import com.homeshoppinglistApp.utilities.SessionUtils;
import java.io.Serializable;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.enterprise.context.RequestScoped;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.servlet.http.HttpSession;
import org.primefaces.context.RequestContext;

/**
 *
 *
 *
 * @author BorisK <klettboris@gmailcom>
 */
@ManagedBean(name = "productBean")
@RequestScoped
public class ProductBean implements Serializable {

    private static final long serialVersionUID = 1094801825228386363L;

    @Inject
    private Service services;
    private HttpSession session;
    private String homeName;
    private String roomCode;
    private List<Product> products = new ArrayList<>();
    private HomeRoom homeRoom;
    private String prodLibelle;
    private String prodCode;
    private UnityQuantityEnum prodUnity;
    private List<UnityQuantityEnum> prodUnitiesList = new ArrayList<>();
    private String prodUnityCode;
    private String prodLimityQuantity;
    private String prodCurentQuantity;
    private String prodAlertQuantity;
    private List<HomeRoomProduct> prodHRProductsList = new ArrayList<>();
    private boolean nomDisabled = false;
    private boolean codeDisabled = false;
    private boolean otherFields = true;
    private boolean modifyButtonRendered = false;
    private boolean otherFieldsRendered = false;
    private Date expirationDate;
    private boolean addButtonDisabled = false;
    private boolean upNTrashButtonsDis = false;
    private String prodUsedLibelle;
    private HomeRoomProduct prodHRProductSelected;
    private String prodSelectedDate;
    private String prodUsedQuantity;
    private String prodDetailTitle;
    private boolean isModification;
    private List<String> productNames = new ArrayList<>();
    List<Product> productsList;

    public ProductBean() {
        this.upNTrashButtonsDis = false;
    }

    @PostConstruct
    public void init() {
        this.upNTrashButtonsDis = false;
        this.postInit();
        productsList = new ArrayList<>();
        productsList = this.services.getProductsListService();

        proNames();
    }

    public void postInit() {
        try {
            session = SessionUtils.getSession();
            if (session != null) {
                this.homeName = (String) this.session.getAttribute("homeName");
                this.roomCode = (String) this.session.getAttribute("roomCode");
            }
        } catch (Exception ex) {
        }

        this.homeRoom = this.services.getHomeRoomByHNRService(this.services.getHomeByNameService(homeName),
                this.services.getRoomByCodeService(roomCode));

        this.prodHRProductsList = new ArrayList<>();
        this.prodHRProductsList.addAll(this.services.getHomeRoomProductsByRoomHomeService(this.homeRoom));
        this.session.setAttribute("prodHRProductsList", this.prodHRProductsList);

        prodUnitiesList = new ArrayList<>();
        this.prodUnitiesList.addAll(this.services.getUnityQuantityListService());

        this.products = new ArrayList<>();
        this.products.addAll(this.services.getProductsListService());

        try {
            this.isModification = (boolean) this.session.getAttribute("isModification");
        } catch (Exception ex) {
            this.isModification = false;
            this.session.setAttribute("isModification", false);
        }

        try {
            if (isModification && !this.prodLibelle.isEmpty()) {
                this.prodDetailTitle = "Modifier le produit: " + this.prodLibelle;
            } else if (isModification) {
                this.isModification = false;
                this.session.setAttribute("isModification", false);
                this.prodDetailTitle = "Ajouter un produit dans l'inventaire";
            } else {
                this.prodDetailTitle = "Ajouter un produit dans l'inventaire";
            }
        } catch (Exception ex) {
            this.prodDetailTitle = "Ajouter un produit dans l'inventaire";
        }
    }

    private void proNames() {
        this.productNames = new ArrayList<>();
        this.productsList = new ArrayList<>();
        this.productsList = this.services.getProductsListService();

        for (Product p : productsList) {
            productNames.add(p.getLibelle());
        }

        this.session.setAttribute("productNames", this.productNames);
    }

    public void disabledButton() {
        this.populateFields();
        this.upNTrashButtonsDis = true;
        RequestContext.getCurrentInstance().update("prodDetailForm");
    }

    public void initPro(Integer p_id) {
        this.postInit();

        HomeRoomProduct hrp = this.services.getHomeRoomProductByIdService(p_id);
        this.prodLibelle = hrp.getProduct().getLibelle();
        this.session.setAttribute("isModification", true);
        this.isModification = true;
        this.prodDetailTitle = "Modifier le produit: " + this.prodLibelle;

        this.prodCode = hrp.getProduct().getCode();
        this.prodAlertQuantity = hrp.getAlertQuantity().toString();
        this.prodCurentQuantity = hrp.getCurrentQuantity().toString();
        this.prodLimityQuantity = hrp.getLimitQuantity().toString();
        this.prodUnityCode = hrp.getProduct().getUnity().getCode();
        this.prodUnity = hrp.getProduct().getUnity();
        this.expirationDate = hrp.getPeremptionDate();
        nomDisabled = true;
        codeDisabled = true;
        this.addButtonDisabled = true;
        upNTrashButtonsDis = false;
        this.session.setAttribute("upNTrashButtonsDis", false);
        this.populateFields();

    }

    public void findProd(String libelle) {
        try {
            if (this.prodLibelle == null || this.prodLibelle.isEmpty() || this.prodLibelle.equalsIgnoreCase("")) {
                this.prodLibelle = libelle;
            }
        } catch (Exception ex) {
            this.prodLibelle = libelle;
        }
        this.findProduct();
        RequestContext.getCurrentInstance().execute("$('.modalPseudoClassProdList').modal('hide')");
    }

    public void findProduct() {
        boolean canContinue = false;

        try {
            if (this.prodLibelle == null || this.prodLibelle.isEmpty() || this.prodLibelle.equalsIgnoreCase("")) {
                RequestContext.getCurrentInstance().execute("$('.modalPseudoClassProdList').modal()");
                return;
            }

            if ((this.prodLibelle != null && !this.prodLibelle.isEmpty() && !this.prodLibelle.equalsIgnoreCase(""))
                    && (this.prodCode != null && !this.prodCode.isEmpty() && !this.prodCode.equalsIgnoreCase(""))) {
                canContinue = true;
            }
        } catch (Exception ex) {
            RequestContext.getCurrentInstance().execute("$('.modalPseudoClassProdList').modal()");
            return;
        }

        this.postInit();
        nomDisabled = false;
        codeDisabled = false;
        this.session.setAttribute("prodLibelle", null);
        this.session.setAttribute("prodCode", null);

        Product prodFinded;

        prodFinded = this.services.getProductByLibelleService(prodLibelle);
        this.otherFields = false;
        this.otherFieldsRendered = true;

        try {
            if (prodFinded == null && (this.prodCode == null || this.prodCode.isEmpty() || this.prodCode.equalsIgnoreCase(""))) {
                FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_WARN, "Attention", "Veuillez saisir le code du nouveau produit");
                FacesContext.getCurrentInstance().addMessage("prodDetailForm:proCode", msg);
                codeDisabled = false;
                return;
            }
        } catch (Exception ex) {
            FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_WARN, "Attention", "Veuillez saisir le code du nouveau produit");
            FacesContext.getCurrentInstance().addMessage("prodDetailForm:proCode", msg);
            codeDisabled = false;
            return;
        }

        try {
            this.prodUnityCode = prodFinded.getUnity().getCode();
        } catch (NullPointerException ex) {
        }

        try {
            if (this.prodUnityCode.equalsIgnoreCase("-- SelectOne --")
                    || this.prodUnityCode.equalsIgnoreCase("")
                    || this.prodUnityCode == null) {
                FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_WARN, "Attention", "Veuillez choisir l'unité de quantité");
                FacesContext.getCurrentInstance().addMessage("prodDetailForm:prodQ", msg);
                codeDisabled = false;
                return;
            } else if (canContinue) {
                FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_WARN, "Attention", "Veuilez cliquer sur \"Ajouter\"");
                FacesContext.getCurrentInstance().addMessage("prodDetailForm:prodMsg", msg);
            }
        } catch (NullPointerException ex) {
            FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_WARN, "Attention", "Veuillez choisir l'unité de quantité");
            FacesContext.getCurrentInstance().addMessage("prodDetailForm:prodQ", msg);
            codeDisabled = false;
            return;
        }

        try {
            this.prodUnity = prodFinded.getUnity();
        } catch (NullPointerException ex) {
        }

        try {
            this.session.setAttribute("unityCode", this.prodUnityCode);
        } catch (NullPointerException ex) {
        }

        try {
            this.prodCode = prodFinded.getCode();
        } catch (NullPointerException ex) {
        }

        try {
            this.session.setAttribute("prodLibelle", this.prodLibelle);
        } catch (NullPointerException ex) {
        }

        try {
            this.session.setAttribute("prodCode", this.prodCode);
        } catch (NullPointerException ex) {
        }
        this.prodCode = this.prodCode.toUpperCase();
        String s1 = this.prodLibelle.substring(0, 1).toUpperCase();
        String s2 = this.prodLibelle.substring(1, this.prodLibelle.length()).toLowerCase();
        this.prodLibelle = s1 + s2;

        nomDisabled = true;
        codeDisabled = true;
    }

    public void reset() {
        this.prodDetailTitle = "Ajouter un produit dans l'inventaire";
        this.inputNullFields();
        this.addButtonDisabled = false;
        upNTrashButtonsDis = true;
        try {
            this.session.setAttribute("upNTrashButtonsDis", true);
        } catch (Exception ex) {
        }
        try {
            this.session.setAttribute("isModification", false);
        } catch (Exception ex) {
        }
        this.prodDetailTitle = "Ajouter un produit dans l'inventaire";
    }

    private void inputNullFields() {
        nomDisabled = false;
        codeDisabled = false;

        this.prodAlertQuantity = null;
        this.prodCode = null;
        this.prodCurentQuantity = null;
        this.prodLibelle = null;
        this.prodLimityQuantity = null;
        this.prodUnityCode = "-- SelectOne --";
        this.expirationDate = null;
        try {
            this.session.setAttribute("cq", null);
        } catch (Exception ex) {
        }

        try {
            this.session.setAttribute("aq", null);
        } catch (Exception ex) {
        }

        try {
            this.session.setAttribute("lq", null);
        } catch (Exception ex) {
        }

        try {
            this.session.setAttribute("prodLibelle", null);
        } catch (Exception ex) {
        }

        try {
            this.session.setAttribute("prodCode", null);
        } catch (Exception ex) {
        }

        try {
            this.session.setAttribute("date", null);
        } catch (Exception ex) {
        }

        this.session.setAttribute("unityCode", "-- SelectOne --");
    }

    private void populateFields() {
        this.postInit();
        try {
            if ((!this.prodUnityCode.equalsIgnoreCase("-- SelectOne --")
                    && !this.prodUnityCode.equalsIgnoreCase("")
                    && !this.prodUnityCode.isEmpty())) {
                this.session.setAttribute("unityCode", this.prodUnityCode);
            }
        } catch (Exception ex) {
            this.prodUnityCode = (String) this.session.getAttribute("unityCode");
        }

        try {
            if ((this.prodUnityCode.equalsIgnoreCase("-- SelectOne --")
                    || this.prodUnityCode.equalsIgnoreCase("")
                    || this.prodUnityCode.isEmpty())) {
                this.prodUnityCode = (String) this.session.getAttribute("unityCode");
            }
        } catch (Exception ex) {
            this.prodUnityCode = (String) this.session.getAttribute("unityCode");
        }

        try {
            if (this.expirationDate != null) {
                this.session.setAttribute("date", this.expirationDate);
            }
        } catch (Exception ex) {
        }

        try {
            if (!this.prodLibelle.isEmpty() || !this.prodLibelle.equalsIgnoreCase("")) {
                this.session.setAttribute("prodLibelle", this.prodLibelle);
            }
        } catch (Exception ex) {
        }

        try {
            if (!this.prodCode.isEmpty() || !this.prodCode.equalsIgnoreCase("")) {
                this.session.setAttribute("prodCode", this.prodCode);
            }
        } catch (Exception ex) {
        }

        try {
            if (!this.prodCurentQuantity.isEmpty() || !this.prodCurentQuantity.equalsIgnoreCase("")) {
                this.session.setAttribute("cq", this.prodCurentQuantity);
            }
        } catch (Exception ex) {
            this.prodCurentQuantity = (String) this.session.getAttribute("cq");
        }

        try {
            if (!this.prodLimityQuantity.isEmpty() || !this.prodLimityQuantity.equalsIgnoreCase("")) {
                this.session.setAttribute("lq", this.prodLimityQuantity);
            }
        } catch (Exception ex) {
            this.prodLimityQuantity = (String) this.session.getAttribute("lq");
        }

        try {
            if (!this.prodAlertQuantity.isEmpty() || !this.prodAlertQuantity.equalsIgnoreCase("")) {
                this.session.setAttribute("aq", this.prodAlertQuantity);
            }
        } catch (Exception ex) {
            this.prodAlertQuantity = (String) this.session.getAttribute("aq");
        }

        try {
            if (this.prodLibelle.isEmpty() || this.prodLibelle.equalsIgnoreCase("") || this.prodLibelle == null) {
                this.prodLibelle = (String) this.session.getAttribute("prodLibelle");
            }
        } catch (Exception ex) {
            this.prodLibelle = (String) this.session.getAttribute("prodLibelle");
        }

        try {
            if (this.expirationDate == null) {
                this.expirationDate = (Date) this.session.getAttribute("date");
            }
        } catch (Exception ex) {
            this.expirationDate = (Date) this.session.getAttribute("date");
        }

        try {
            if (this.prodCode.isEmpty() || this.prodCode.equalsIgnoreCase("")) {
                this.prodCode = (String) this.session.getAttribute("prodCode");
            }
        } catch (Exception ex) {
            this.prodCode = (String) this.session.getAttribute("prodCode");
        }
    }

    private boolean checkQuantities() {
        boolean isOk = true;
        this.populateFields();

        if (Integer.valueOf(this.prodCurentQuantity) < Integer.valueOf(this.prodAlertQuantity)) {
            FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_ERROR, "ERREUR: ", "la quantité disponible du produit doit être suppérieur à celle d'alerte");
            FacesContext.getCurrentInstance().addMessage("prodDetailForm:prodMsg", msg);

            msg = new FacesMessage(FacesMessage.SEVERITY_ERROR, "ERREUR: ", "Quantité disponible");
            FacesContext.getCurrentInstance().addMessage("prodDetailForm:prodAQ", msg);

            msg = new FacesMessage(FacesMessage.SEVERITY_ERROR, "ERREUR: ", "Quantité alerte");
            FacesContext.getCurrentInstance().addMessage("prodDetailForm:prodALQ", msg);
            this.findProduct();
            return false;
        }

        if (Integer.valueOf(this.prodCurentQuantity) < Integer.valueOf(this.prodLimityQuantity)) {
            FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_ERROR, "ERREUR: ", "la quantité disponible du produit doit être suppérieur à la limite");
            FacesContext.getCurrentInstance().addMessage("prodDetailForm:prodMsg", msg);

            msg = new FacesMessage(FacesMessage.SEVERITY_ERROR, "ERREUR: ", "Quantité limite");
            FacesContext.getCurrentInstance().addMessage("prodDetailForm:prodLQ", msg);

            msg = new FacesMessage(FacesMessage.SEVERITY_ERROR, "ERREUR: ", "Quantité disponible");
            FacesContext.getCurrentInstance().addMessage("prodDetailForm:prodAQ", msg);
            this.findProduct();
            return false;
        }

        if (Integer.valueOf(this.prodLimityQuantity) > Integer.valueOf(this.prodAlertQuantity)) {
            FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_ERROR, "ERREUR: ", "la quantité alerte du produit doit être suppérieur à la limite");
            FacesContext.getCurrentInstance().addMessage("prodDetailForm:prodMsg", msg);

            msg = new FacesMessage(FacesMessage.SEVERITY_ERROR, "ERREUR: ", "Quantité limite");
            FacesContext.getCurrentInstance().addMessage("prodDetailForm:prodLQ", msg);

            msg = new FacesMessage(FacesMessage.SEVERITY_ERROR, "ERREUR: ", "Quantité alerte");
            FacesContext.getCurrentInstance().addMessage("prodDetailForm:prodALQ", msg);
            this.findProduct();
            return false;
        }

        return isOk;
    }

    public TimeZone getTimeZone() {
        return TimeZone.getDefault();
    }

    public void deleteHRP() {
        this.populateFields();
        if (!this.isModification || (boolean) this.session.getAttribute("isModification") == false) {
            if (this.checkFields()) {
                FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_ERROR, "ERREUR: ", "Veuillez cliquer sur \"Ajouter\"");
                FacesContext.getCurrentInstance().addMessage("prodDetailForm:prodMsg", msg);
                this.nomDisabled = true;
                this.codeDisabled = true;
            }
            this.proNames();
            return;
        }

        HomeRoomProduct hRP = this.services.getHomeRoomProductByHNPService(homeRoom,
                this.services.getProductByLibelleService(this.prodLibelle));

        this.services.removeProductFromHomeRoomService(hRP);
        FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_INFO, "INFO: ", "Le produit a été supprimé dans l'inventaire");
        FacesContext.getCurrentInstance().addMessage("prodDetailForm:prodMsg", msg);

        this.reset();
    }

    private boolean checkExpireDate() {

        this.populateFields();
        boolean isOk = true;
        try {

            Date date1 = Service.FORMATTER.parse(Service.FORMATTER.format(this.expirationDate));
            Date date2 = Service.FORMATTER.parse(Service.FORMATTER.format(new Date()));

            if (date1.compareTo(date2) < 0) {
                FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_ERROR, "ERREUR: ", "La date de peremption ne peut être une date échue");
                FacesContext.getCurrentInstance().addMessage("prodDetailForm:prodDP", msg);
                this.findProduct();
                isOk = false;
            }

        } catch (ParseException ex) {
            Logger.getLogger(ProductBean.class.getName()).log(Level.SEVERE, null, ex);
        }
        return isOk;
    }

    public void addProduct() {
        this.populateFields();
        if (this.isModification || (boolean) this.session.getAttribute("isModification") == true) {
            if (this.checkFields()) {
                FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_ERROR, "ERREUR: ", "Veuillez cliquer sur \"Sauvegarder\" ou sur \"Supprimer\"");
                FacesContext.getCurrentInstance().addMessage("prodDetailForm:prodMsg", msg);
                this.nomDisabled = true;
                this.codeDisabled = true;
            }
            this.proNames();
            return;
        }
        try {

            if (IsProductExist()) {
                this.populateFields();
                FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_ERROR, "ERREUR: ", "Impossible de faire l'ajout. Ce produit existe déjà dans l'inventaire des produits de la pièce");
                FacesContext.getCurrentInstance().addMessage("prodDetailForm:prodMsg",
                        msg);
                this.prodCode = this.services.getProductByLibelleService(this.prodLibelle).getCode();
                return;
            }

            if (!checkFields()) {
                return;
            }

            if (!this.checkExpireDate()) {
                this.findProduct();
                return;
            }

            if (!this.checkQuantities()) {
                this.findProduct();
                return;
            }

            Product prodFinded
                    = this.services.getProductByLibelleService(prodLibelle);
            if (prodFinded
                    == null) {
                if (this.services.getProductByCodeService(this.prodCode)
                        != null) {
                    FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_ERROR, "ERREUR: ", "Le code: "
                            + this.prodCode + ", existe déjà");
                    FacesContext.getCurrentInstance().addMessage("prodDetailForm:proCode",
                            msg);
                    this.nomDisabled = false;
                    this.codeDisabled = false;
                    return;
                }
                String chaine = this.prodLibelle.toLowerCase();
                char[] char_table = chaine.toCharArray();
                char_table[0]
                        = Character.toUpperCase(char_table[0]);
                chaine = new String(char_table);

                prodFinded = new Product();
                prodFinded.setCode(this.prodCode.toUpperCase());
                prodFinded.setLibelle(chaine);
                prodFinded.setUnity(this.services.getUnityQuantityByCodeService(this.prodUnityCode));
                this.services.createNewProductService(prodFinded);
            }

            String date = Service.FORMATTER.format(expirationDate);
            this.services.addProductToHomeRoomService(this.homeRoom,
                    this.services.getProductByCodeService(this.prodCode),
                    Integer.valueOf(this.prodAlertQuantity),
                    Integer.valueOf(this.prodCurentQuantity),
                    Integer.valueOf(this.prodLimityQuantity),
                    Service.FORMATTER.parse(date));
            FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_INFO, "INFO: ", "Le nouveau produit a été ajouté dans l'inventaire");
            FacesContext.getCurrentInstance().addMessage("prodDetailForm:prodMsg",
                    msg);

            this.reset();
        } catch (ParseException ex) {
            Logger.getLogger(ProductBean.class.getName()).log(Level.SEVERE, null,
                    ex);
        }

    }

    public void modifyProduct() {
        this.populateFields();
        if (!this.isModification || (boolean) this.session.getAttribute("isModification") == false) {
            if (this.checkFields()) {
                FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_ERROR, "ERREUR: ", "Veuillez cliquer sur \"Ajouter\"");
                FacesContext.getCurrentInstance().addMessage("prodDetailForm:prodMsg", msg);
                this.nomDisabled = true;
                this.codeDisabled = true;
            }
            this.proNames();
            return;
        }

        try {

            if (!checkFields()) {
                return;
            }

            if (!IsProductExist()) {
                this.populateFields();
                FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_ERROR, "ERREUR: ", "Impossible de faire une mise à jour. Ce produit n'existe pas dans l'inventaire des produits de la pièce");
                FacesContext.getCurrentInstance().addMessage("prodDetailForm:prodMsg", msg);
                this.findProduct();
                return;
            }

            if (!this.checkExpireDate()) {
                this.findProduct();
                return;
            }

            if (!this.checkQuantities()) {
                this.findProduct();
                return;
            }
            String date = Service.FORMATTER.format(expirationDate);
            HomeRoomProduct hrp = this.services.getHomeRoomProductByHNPService(this.homeRoom, this.services.getProductByCodeService(this.prodCode));
            hrp.setAlertQuantity(Integer.valueOf(this.prodAlertQuantity));
            hrp.setCurrentQuantity(Integer.valueOf(this.prodCurentQuantity));
            hrp.setLimitQuantity(Integer.valueOf(this.prodLimityQuantity));
            hrp.setPeremptionDate(Service.FORMATTER.parse(date));

            this.services.updateHomeRoomProductService(hrp);

            FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_INFO, "INFO: ", "Le détail du produit a été modifié");
            FacesContext.getCurrentInstance().addMessage("prodDetailForm:prodMsg", msg);

            this.reset();
        } catch (ParseException ex) {
            Logger.getLogger(ProductBean.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    public boolean IsProductExist() {
        this.populateFields();

        boolean isOk = false;
        for (HomeRoomProduct hmr : this.prodHRProductsList) {
            if (hmr.getProduct().getLibelle().equalsIgnoreCase(this.prodLibelle)) {
                isOk = true;
            }
        }
        return isOk;
    }

    public boolean checkFields() {

        boolean isOk = true;
        this.populateFields();

        String l = "";
        String c = "";

        try {
            if (!this.prodLibelle.isEmpty() || !this.prodLibelle.equalsIgnoreCase("")) {
                l = this.prodLibelle;
            }
        } catch (Exception ex) {
        }

        try {
            if (!this.prodCode.isEmpty() || !this.prodCode.equalsIgnoreCase("")) {
                c = this.prodCode;
            }
        } catch (Exception ex) {
        }

        try {
            if ((l.isEmpty() || l.equalsIgnoreCase(""))
                    && (c.isEmpty() || c.equalsIgnoreCase(""))
                    && (this.prodAlertQuantity.isEmpty() || this.prodAlertQuantity.equalsIgnoreCase("") || this.prodAlertQuantity == null)
                    && (this.prodCurentQuantity.isEmpty() || this.prodCurentQuantity.equalsIgnoreCase("") || this.prodCurentQuantity == null)
                    && (this.prodLimityQuantity.isEmpty() || this.prodLimityQuantity.equalsIgnoreCase("") || this.prodLimityQuantity == null)
                    && (this.expirationDate == null)
                    && (this.prodUnityCode.equalsIgnoreCase("-- SelectOne --") || this.prodUnityCode.equalsIgnoreCase("") || this.prodUnityCode.isEmpty())) {
                FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_ERROR, "ERREUR: ", "Veuillez saisir tous les champs");
                FacesContext.getCurrentInstance().addMessage("prodDetailForm:prodMsg", msg);
                return false;
            }
        } catch (Exception ex) {
            FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_ERROR, "ERREUR: ", "Veuillez saisir tous les champs");
            FacesContext.getCurrentInstance().addMessage("prodDetailForm:prodMsg", msg);
            return false;
        }

        try {
            if ((this.prodLibelle.isEmpty() || this.prodLibelle.equalsIgnoreCase(""))) {
                FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_ERROR, "ERREUR: ", "Veuillez saisir le nom du produit et lancer la recherche sur le produit");
                FacesContext.getCurrentInstance().addMessage("prodDetailForm:prodLibelle", msg);
                isOk = false;
            }
        } catch (Exception ex) {
            FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_ERROR, "ERREUR: ", "Veuillez saisir le nom du produit et lancer la recherche sur le produit");
            FacesContext.getCurrentInstance().addMessage("prodDetailForm:prodLibelle", msg);
            isOk = false;
        }

        try {
            if (this.prodCode.isEmpty() || this.prodCode.equalsIgnoreCase("")) {
                this.prodCode = (String) this.session.getAttribute("prodCode");
            }
        } catch (Exception ex) {
            this.prodCode = (String) this.session.getAttribute("prodCode");
        }
        try {
            if ((this.prodCode.isEmpty() || this.prodCode.equalsIgnoreCase(""))) {
                FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_ERROR, "ERREUR: ", "Veuillez faire une recherche sur le nom du produit saisi et saisir un code en cas de nouveau produit");
                FacesContext.getCurrentInstance().addMessage("prodDetailForm:proCode", msg);
                isOk = false;
            }
        } catch (Exception ex) {
            FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_ERROR, "ERREUR: ", "Veuillez faire une recherche sur le nom du produit saisi et saisir un code en cas de nouveau produit");
            FacesContext.getCurrentInstance().addMessage("prodDetailForm:proCode", msg);
            isOk = false;
        }

        try {
            if ((this.prodUnityCode.equalsIgnoreCase(
                    "-- SelectOne --") || this.prodUnityCode.equalsIgnoreCase("") || this.prodUnityCode.isEmpty())) {
                FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_ERROR, "ERREUR: ", "Veuillez choisir l'unité de quantité");
                FacesContext.getCurrentInstance().addMessage("prodDetailForm:prodQ", msg);
                isOk = false;
            }
        } catch (Exception ex) {
            this.prodUnityCode = "-- SelectOne --";
            FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_ERROR, "ERREUR: ", "Veuillez choisir l'unité de quantité");
            FacesContext.getCurrentInstance().addMessage("prodDetailForm:prodQ", msg);
            isOk = false;
        }

        if (isOk) {
            this.nomDisabled = true;
            this.codeDisabled = true;
        }

        try {
            if (this.prodAlertQuantity.isEmpty() || this.prodAlertQuantity.equalsIgnoreCase("")) {
                this.prodAlertQuantity = (String) this.session.getAttribute("aq");
            }
        } catch (Exception ex) {
            this.prodAlertQuantity = (String) this.session.getAttribute("aq");
        }

        try {
            if ((this.prodAlertQuantity.isEmpty() || this.prodAlertQuantity.equalsIgnoreCase("") || this.prodAlertQuantity == null)) {
                FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_ERROR, "ERREUR: ", "Veuillez saisir la quantité alerte");
                FacesContext.getCurrentInstance().addMessage("prodDetailForm:prodALQ", msg);
                isOk = false;
            }
        } catch (Exception ex) {
            FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_ERROR, "ERREUR:", "Veuillez saisir la quantité alerte");
            FacesContext.getCurrentInstance().addMessage("prodDetailForm:prodALQ", msg);
            isOk = false;
        }

        try {
            if (this.prodLimityQuantity.isEmpty() || this.prodLimityQuantity.equalsIgnoreCase("")) {
                this.prodLimityQuantity = (String) this.session.getAttribute("lq");
            }
        } catch (Exception ex) {
            this.prodLimityQuantity = (String) this.session.getAttribute("lq");
        }

        try {
            if ((this.prodLimityQuantity.isEmpty() || this.prodLimityQuantity.equalsIgnoreCase(""))) {
                FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_ERROR, "ERREUR: ", "Veuillez saisir la quantité limite");
                FacesContext.getCurrentInstance().addMessage("prodDetailForm:prodLQ", msg);
                isOk = false;
            }
        } catch (Exception ex) {
            FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_ERROR, "ERREUR:", "Veuillez saisir la quantité limite");
            FacesContext.getCurrentInstance().addMessage("prodDetailForm:prodLQ", msg);
            isOk = false;
        }

        try {
            if (this.expirationDate == null) {
                this.expirationDate = (Date) this.session.getAttribute("date");
            }
        } catch (Exception ex) {
            this.expirationDate = (Date) this.session.getAttribute("date");
        }

        try {
            if ((this.expirationDate == null)) {
                FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_ERROR, "ERREUR: ", "Veuillez saisir la date de peremption");
                FacesContext.getCurrentInstance().addMessage("prodDetailForm:prodDP", msg);
                isOk = false;
            }
        } catch (Exception ex) {
            FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_ERROR, "ERREUR: ", "Veuillez saisir la date de peremption");
            FacesContext.getCurrentInstance().addMessage("prodDetailForm:prodDP", msg);
            isOk = false;
        }

        try {
            if (this.prodCurentQuantity.isEmpty() || this.prodCurentQuantity.equalsIgnoreCase("")) {
                this.prodCurentQuantity = (String) this.session.getAttribute("cq");
            }
        } catch (Exception ex) {
            this.prodCurentQuantity = (String) this.session.getAttribute("cq");;
        }

        try {
            if ((this.prodCurentQuantity.isEmpty() || this.prodCurentQuantity.equalsIgnoreCase(""))) {
                FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_ERROR, "ERREUR: ", "Veuillez saisir la quantité disponible");
                FacesContext.getCurrentInstance().addMessage("prodDetailForm:prodAQ", msg);
                isOk = false;
            }
        } catch (Exception ex) {
            FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_ERROR, "ERREUR: ", "Veuillez saisir la quantité disponible");
            FacesContext.getCurrentInstance().addMessage("prodDetailForm:prodAQ", msg);
            isOk = false;
        }

        return isOk;

    }

//------------------------------------ Getter and Setter -------------------------------------------------
    public String getProdDetailTitle() {
        return prodDetailTitle;
    }

    public void setProdDetailTitle(String prodDetailTitle) {
        this.prodDetailTitle = prodDetailTitle;
    }

    public String getProdUsedQuantity() {
        return prodUsedQuantity;
    }

    public void setProdUsedQuantity(String prodUsedQuantity) {
        this.prodUsedQuantity = prodUsedQuantity;
    }

    public String getProdSelectedDate() {
        return prodSelectedDate;
    }

    public void setProdSelectedDate(String prodSelectedDate) {
        this.prodSelectedDate = prodSelectedDate;
    }

    public HomeRoomProduct getProdHRProductSelected() {
        return prodHRProductSelected;
    }

    public void setProdHRProductSelected(HomeRoomProduct prodHRProductSelected) {
        this.prodHRProductSelected = prodHRProductSelected;
    }

    public String getProdUsedLibelle() {
        return prodUsedLibelle;
    }

    public void setProdUsedLibelle(String prodUsedLibelle) {
        this.prodUsedLibelle = prodUsedLibelle;
    }

    public boolean isUpNTrashButtonsDis() {
        return upNTrashButtonsDis;
    }

    public void setUpNTrashButtonsDis(boolean upNTrashButtonsDis) {
        this.upNTrashButtonsDis = upNTrashButtonsDis;
    }

    public boolean isAddButtonDisabled() {
        return addButtonDisabled;
    }

    public void setAddButtonDisabled(boolean addButtonDisabled) {
        this.addButtonDisabled = addButtonDisabled;
    }

    public Date getExpirationDate() {
        return expirationDate;
    }

    public void setExpirationDate(Date expirationDate) {
        this.expirationDate = expirationDate;
    }

    public boolean isOtherFieldsRendered() {
        return otherFieldsRendered;
    }

    public void setOtherFieldsRendered(boolean otherFieldsRendered) {
        this.otherFieldsRendered = otherFieldsRendered;
    }

    public boolean isModifyButtonRendered() {
        return modifyButtonRendered;
    }

    public void setModifyButtonRendered(boolean modifyButtonRendered) {
        this.modifyButtonRendered = modifyButtonRendered;
    }

    public boolean isOtherFields() {
        return otherFields;
    }

    public void setOtherFields(boolean otherFields) {
        this.otherFields = otherFields;
    }

    public boolean isCodeDisabled() {
        return codeDisabled;
    }

    public void setCodeDisabled(boolean codeDisabled) {
        this.codeDisabled = codeDisabled;
    }

    public boolean isNomDisabled() {
        return nomDisabled;
    }

    public void setNomDisabled(boolean nomDisabled) {
        this.nomDisabled = nomDisabled;
    }

    public List<HomeRoomProduct> getProdHRProductsList() {
        this.postInit();
        return prodHRProductsList;
    }

    public void setProdHRProductsList(List<HomeRoomProduct> prodHRProductsList) {
        this.prodHRProductsList = prodHRProductsList;
    }

    public String getProdLimityQuantity() {
        this.postInit();
        return prodLimityQuantity;
    }

    public void setProdLimityQuantity(String prodLimityQuantity) {
        this.prodLimityQuantity = prodLimityQuantity;
    }

    public String getProdCurentQuantity() {
        this.postInit();
        return prodCurentQuantity;
    }

    public void setProdCurentQuantity(String prodCurentQuantity) {
        this.prodCurentQuantity = prodCurentQuantity;
    }

    public String getProdAlertQuantity() {
        return prodAlertQuantity;
    }

    public void setProdAlertQuantity(String prodAlertQuantity) {
        this.prodAlertQuantity = prodAlertQuantity;
    }

    public String getProdUnityCode() {
        this.postInit();
        return prodUnityCode;
    }

    public void setProdUnityCode(String prodUnityCode) {
        this.prodUnityCode = prodUnityCode;
    }

    public List<UnityQuantityEnum> getProdUnitiesList() {
        this.postInit();
        return prodUnitiesList;
    }

    public void setProdUnitiesList(List<UnityQuantityEnum> prodUnitiesList) {
        this.prodUnitiesList = prodUnitiesList;
    }

    public UnityQuantityEnum getProdUnity() {
        this.postInit();
        return prodUnity;
    }

    public void setProdUnity(UnityQuantityEnum prodUnity) {
        this.prodUnity = prodUnity;
    }

    public String getProdLibelle() {
        this.postInit();
        return prodLibelle;
    }

    public void setProdLibelle(String prodLibelle) {
        this.prodLibelle = prodLibelle;
    }

    public String getProdCode() {
        this.postInit();
        return prodCode;
    }

    public void setProdCode(String prodCode) {
        this.prodCode = prodCode;
    }

    public List<Product> getProducts() {
        this.postInit();
        return products;
    }

    public void setProducts(List<Product> products) {
        this.products = products;
    }

    public List<String> getProductNames() {
        this.proNames();
        if (productNames.isEmpty()) {
            this.productNames = (List<String>) this.session.getAttribute("productNames");
        }
        return productNames;
    }

    public void setProductNames(List<String> productNames) {
        this.productNames = productNames;
        if (productNames.isEmpty()) {
            this.productNames = (List<String>) this.session.getAttribute("productNames");
        }
    }

}
