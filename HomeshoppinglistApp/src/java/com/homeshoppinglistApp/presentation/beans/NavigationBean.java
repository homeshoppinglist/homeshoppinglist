package com.homeshoppinglistApp.presentation.beans;

import java.io.Serializable;
import javax.enterprise.context.RequestScoped;
import javax.faces.bean.ManagedBean;

/**
 *
 * @author BorisK<klettboris@gmail.com>
 */
@ManagedBean(name = "navigationBean")
@RequestScoped
public class NavigationBean implements Serializable {

    private static final long serialVersionUID = 1094801825228386363L;

    public NavigationBean() {
    }

    public String backToWelcomePage() {
        return "success";
    }

    public String backToMainPage() {
        return "success";
    }
}
