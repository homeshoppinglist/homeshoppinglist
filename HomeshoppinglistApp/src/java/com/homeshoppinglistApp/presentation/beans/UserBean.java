package com.homeshoppinglistApp.presentation.beans;

import com.homeshoppinglistApp.business.Membre;
import com.homeshoppinglistApp.business.User;
import com.homeshoppinglistApp.services.Service;
import com.homeshoppinglistApp.utilities.SessionUtils;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.enterprise.context.RequestScoped;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.servlet.http.HttpSession;
import org.primefaces.context.RequestContext;

/**
 *
 * @author BorisK <klettboris@gmail.com>
 */
@ManagedBean(name = "userBean")
@RequestScoped
public class UserBean implements Serializable {

    private static final long serialVersionUID = 1094801825228386363L;

    @Inject
    private Service services;

    private String firstname;
    private String lastname;
    private String emailAdd;
    private Date birthday;
    private HttpSession session;

    public UserBean() {
    }

    @PostConstruct
    public void init() {
        try {
            session = SessionUtils.getSession();
            if (session != null) {
                User user = new User();
                String email = (String) this.session.getAttribute("userEmail");
                user = this.services.getUserByEmailService(email);
                this.firstname = user.getFirstname();
                this.lastname = user.getLastname();
                this.birthday = user.getBirthdate();
                this.emailAdd = user.getEmail();
            }
        } catch (Exception ex) {
        }
    }

    public void modify() {
        if (checkUserAlreadyExiste()) {
            return;
        }

        User user = new User();
        user = this.services.getUserByEmailService(this.emailAdd);
        user.setBirthdate(birthday);
        user.setFirstname(firstname);
        user.setLastname(lastname);
        this.services.updateUserService(user);
        RequestContext.getCurrentInstance().update("initForm");

    }

    private boolean checkUserAlreadyExiste() {
        return this.services.getUserByInfosService(firstname, lastname, birthday) != null;
    }

    public void createNewUser() {
        if (firstname.isEmpty()
                || lastname.isEmpty()
                || emailAdd.isEmpty()) {
            FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_ERROR, "", "Veuillez Veuillez saisir tous les champs correctement.");
            FacesContext.getCurrentInstance().addMessage("loginForm:connexionTabView:newUserMsg", msg);
            return;
        }

        if (checkUserAlreadyExiste()) {
            FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_ERROR, "", "Cet utilisateur existe déjà.");
            FacesContext.getCurrentInstance().addMessage("loginForm:connexionTabView:newUserMsg", msg);
            return;
        }

        if (this.services.getUserByEmailService(emailAdd) != null) {
            FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_ERROR, "", "L'adresse email: " + this.emailAdd + " existe déjà.");
            FacesContext.getCurrentInstance().addMessage("loginForm:connexionTabView:newUserMsg", msg);
            return;
        }

        int random = 0 + (int) (Math.random() * ((100 - 0) + 1));
        String pwd = this.firstname.substring(0, 1).toUpperCase() + this.firstname.substring(1, 2) + random + this.lastname.substring(0, 2);
        User user = new User(firstname, lastname, emailAdd, pwd, birthday, new ArrayList<>());
        this.services.createNewUserService(user);

        this.firstname = null;
        this.lastname = null;
        this.birthday = null;
        this.emailAdd = null;

    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getEmailAdd() {
        return emailAdd;
    }

    public void setEmailAdd(String emailAdd) {
        this.emailAdd = emailAdd;
    }

    public Date getBirthday() {
        return birthday;
    }

    public void setBirthday(Date birthday) {
        this.birthday = birthday;
    }

}
