package com.homeshoppinglistApp.presentation.beans;

import com.homeshoppinglistApp.business.Home;
import com.homeshoppinglistApp.business.HomeRoom;
import com.homeshoppinglistApp.business.HomeRoomProduct;
import com.homeshoppinglistApp.business.RoomEnum;
import com.homeshoppinglistApp.services.Service;
import com.homeshoppinglistApp.utilities.SessionUtils;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.enterprise.context.RequestScoped;
import javax.faces.bean.ManagedBean;
import javax.inject.Inject;
import javax.servlet.http.HttpSession;
import org.primefaces.context.RequestContext;

/**
 *
 * @author BorisK<klettboris@gmail.com>
 */
@ManagedBean(name = "homeRoomBean")
@RequestScoped
public class HomeRoomBean implements Serializable {

    private static final long serialVersionUID = 1094801825228386363L;

    @Inject
    private Service services;

    private HttpSession session;
    private Home home;
    private List<RoomEnum> roomsList = new ArrayList<>();
    private RoomEnum room;
    private String roomCode;
    private String roomDescription;
    private List<RoomEnum> roomList = new ArrayList<>();
    private boolean isAddRoomButtonRendered;
    private String delRoomConfMess;
    private HomeRoom hr;

    public HomeRoomBean() {
    }

    @PostConstruct
    public void init() {
        this.postInit();
    }

    public void deletRoomFromHome(Integer p_roomId) {
        delRoomConfMess = "Souhaitez-vous supprimer cette pièce?";
        this.hr = this.services.getHomeRoomByHNRService(this.home, this.services.getRoomByIdService(p_roomId));
        this.session.setAttribute("hr", hr);
        List<HomeRoomProduct> hrpl = new ArrayList<>();
        hrpl = this.services.getHomeRoomProductsByRoomHomeService(this.hr);
        if (!hrpl.isEmpty()) {
            delRoomConfMess = "Cette pièce possède actuellement plusieurs ressources! Souhaitez-vous supprimer cette pièce?";
        }

        RequestContext.getCurrentInstance().update("oneRoomForm");
        RequestContext.getCurrentInstance().update("roomListForm");
        RequestContext.getCurrentInstance().update("myFormDelRoom");
        RequestContext.getCurrentInstance().execute("$('.modalDelRoom').modal()");

    }

    public void deletRoomFromHomeConfirmation() {
        this.services.deleteHomeRoomService((HomeRoom) this.session.getAttribute("hr"));
        RequestContext.getCurrentInstance().update("oneRoomForm");
        RequestContext.getCurrentInstance().update("roomListForm");
        RequestContext.getCurrentInstance().update("myFormDelRoom");
        this.hr = null;
        this.postInit();
        RequestContext.getCurrentInstance().execute("$('.modalDelRoom').modal('hide')");
    }

    public String goToRoomPage(Integer p_roomId) {
        this.session.setAttribute("roomCode", this.services.getRoomByIdService(p_roomId).getCode());
        return "success";
    }

    public void postInit() {

        try {
            session = SessionUtils.getSession();
            this.home = new Home();
            this.home = this.services.getHomeByNameService((String) session.getAttribute("homeName"));
        } catch (Exception ex) {
        }

        List<RoomEnum> hl2 = new ArrayList<>();
        List<HomeRoom> homeRoomList = this.services.getHomeRoomsByHomeService(home);

        this.roomsList = new ArrayList<>();
        homeRoomList.forEach((hr) -> {
            this.roomsList.add(hr.getRoom());
        });

        this.roomList = new ArrayList<>();
        hl2.addAll(this.services.getRoomsListService());
        boolean isOk;

        for (RoomEnum re : hl2) {
            isOk = true;
            for (RoomEnum re1 : this.roomsList) {
                if (re.getCode().equalsIgnoreCase(re1.getCode())) {
                    isOk = false;
                }
            }
            if (isOk) {
                this.roomList.add(re);
            }
        }
        this.roomDescription = "-- SelectOne --";
    }

    public void roomSelected() {

        if (this.roomCode.equalsIgnoreCase("-- SelectOne --")) {
            this.roomCode = "";
            this.isAddRoomButtonRendered = true;
            this.room = null;
        } else {
            this.roomList.stream().filter((re) -> (re.getCode().equalsIgnoreCase(this.roomCode))).map((re) -> {
                this.session.setAttribute("roomCode", this.roomCode);
                this.room = re;
                return re;
            }).map((_item) -> {
                this.isAddRoomButtonRendered = false;
                return _item;
            }).map((_item) -> {
                this.roomDescription = this.room.getDescription();
                return _item;
            }).forEachOrdered((_item) -> {
                this.roomCode = this.room.getCode();
            });

        }
        RequestContext.getCurrentInstance().update("oneRoomForm");
    }

    public void cleanCode() {
        HomeRoom hr = new HomeRoom();
        hr.setHome(home);
        hr.setRoom(this.services.getRoomByCodeService((String) this.session.getAttribute("roomCode")));
        HomeRoom homeRoom = this.services.addHomeRoomService(hr);
        this.postInit();

        List<RoomEnum> hl2 = new ArrayList<>();

        boolean isOk;

        this.roomList = new ArrayList<>();
        hl2.addAll(this.services.getRoomsListService());

        for (RoomEnum re : hl2) {
            isOk = true;
            for (RoomEnum re1 : this.roomsList) {
                if (re.getCode().equalsIgnoreCase(re1.getCode())) {
                    isOk = false;
                }
            }
            if (isOk) {
                this.roomList.add(re);
            }
        }

        this.roomDescription = "-- SelectOne --";
        this.isAddRoomButtonRendered = true;
        this.room = null;
        this.roomCode = null;
        RequestContext.getCurrentInstance().update("oneRoomForm roomListForm:roomList");
    }

    public String getDelRoomConfMess() {
        return delRoomConfMess;
    }

    public void setDelRoomConfMess(String delRoomConfMess) {
        this.delRoomConfMess = delRoomConfMess;
    }

    public String getRoomCode() {
        try {
            roomCode = this.room.getCode();
            this.isAddRoomButtonRendered = false;
        } catch (NullPointerException ex) {
            this.isAddRoomButtonRendered = true;
            return roomCode;

        }
        return roomCode;
    }

    public void setRoomCode(String roomCode) {
        this.roomCode = roomCode;
    }

    public String getRoomDescription() {
        try {
            roomDescription = this.room.getDescription();
            this.isAddRoomButtonRendered = false;
        } catch (NullPointerException ex) {
            this.isAddRoomButtonRendered = true;
            return roomDescription;

        }
        return roomDescription;
    }

    public void setRoomDescription(String roomDescription) {
        this.roomDescription = roomDescription;
    }

    public List<RoomEnum> getRoomsList() {
        return roomsList;
    }

    public void setRoomsList(List<RoomEnum> roomsList) {
        this.roomsList = roomsList;
    }

    public RoomEnum getRoom() {
        return room;
    }

    public void setRoom(RoomEnum room) {
        this.room = room;
    }

    public List<RoomEnum> getRoomList() {
        return this.roomList;
    }

    public void setRoomList(List<RoomEnum> roomList) {
        this.roomList = roomList;
    }

    public boolean isIsAddRoomButtonRendered() {
        return isAddRoomButtonRendered;
    }

    public void setIsAddRoomButtonRendered(boolean isAddRoomButtonRendered) {
        this.isAddRoomButtonRendered = isAddRoomButtonRendered;
    }

}
