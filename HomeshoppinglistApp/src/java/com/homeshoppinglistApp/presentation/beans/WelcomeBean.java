package com.homeshoppinglistApp.presentation.beans;

import com.homeshoppinglistApp.business.Home;
import com.homeshoppinglistApp.business.HomeRoom;
import com.homeshoppinglistApp.business.Membre;
import com.homeshoppinglistApp.business.User;
import com.homeshoppinglistApp.business.UserStatusEnum;
import com.homeshoppinglistApp.services.Service;
import com.homeshoppinglistApp.utilities.SessionUtils;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.enterprise.context.RequestScoped;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.servlet.http.HttpSession;
import org.primefaces.context.RequestContext;

/**
 *
 *
 * @author BorisK <klettboris@gmailcom>
 */
@ManagedBean(name = "welcomeBean")
@RequestScoped
public class WelcomeBean implements Serializable {

    private static final long serialVersionUID = 1094801825228386363L;

    @Inject
    private Service services;

    private String homeName;
    private String homeStreet;
    private String homeZipCode;
    private String homeCity;
    private List<Home> homes = new ArrayList<>();
    private String email;
    HttpSession session;
    private List<Membre> membersShip = new ArrayList<>();

    public WelcomeBean() {

    }

    @PostConstruct
    public void init() {
        this.session = SessionUtils.getSession();
        this.returnUserEmail();
        this.returnUserHomes();
        this.returnMembersShip();

    }

    private void returnMembersShip() {
        this.returnUserEmail();
        this.membersShip = new ArrayList<>();
        User user = new User();
        user = this.services.getUserByEmailService(this.email);
        this.membersShip.addAll(user.getMembers());
        RequestContext.getCurrentInstance().update("myFormAccount:homesList");
    }

    public void ravAccountModal() {
        this.returnMembersShip();
        RequestContext.getCurrentInstance().execute("$('.modalAccount').modal()");

    }

    private void returnUserEmail() {
        this.email = (String) session.getAttribute("userEmail");
    }

    public void returnUserHomes() {

        if (email != null && services != null) {

            //Loop for
            this.homes = new ArrayList<>();
            List<Membre> membList = new ArrayList<>();
            membList.addAll(this.services.getCurrentMembersListByEmailService(email));

            membList.forEach((m) -> {
                this.homes.add(m.getHome());
            });
        }
    }

    private void populateFields() {
        try {
        } catch (Exception ex) {
        }

        try {
            if (this.homeCity.isEmpty() || this.homeCity.equalsIgnoreCase("")) {
                this.homeCity = (String) this.session.getAttribute("homeCity");
            }
            this.session.setAttribute("homeCity", this.homeCity);
        } catch (Exception ex) {
            this.homeCity = (String) this.session.getAttribute("homeCity");
        }

        try {
            if (this.homeName.isEmpty() || this.homeName.equalsIgnoreCase("")) {
                this.homeName = (String) this.session.getAttribute("homeName");
            }

            this.session.setAttribute("homeName", this.homeName);
        } catch (Exception ex) {
            this.homeName = (String) this.session.getAttribute("homeName");
        }

        try {

            if (this.homeStreet.isEmpty() || this.homeStreet.equalsIgnoreCase("")) {
                this.homeStreet = (String) this.session.getAttribute("homeStreet");
            }

            this.session.setAttribute("homeStreet", this.homeStreet);
        } catch (Exception ex) {
            this.homeStreet = (String) this.session.getAttribute("homeStreet");
        }

        try {
            if (this.homeZipCode.isEmpty() || this.homeZipCode.equalsIgnoreCase("")) {
                this.homeZipCode = (String) this.session.getAttribute("homeZipCode");
            }

            this.session.setAttribute("homeZipCode", this.homeZipCode);
        } catch (Exception ex) {
            this.homeZipCode = (String) this.session.getAttribute("homeZipCode");
        }
    }

    public void createNewHome() {
        this.returnUserEmail();
        this.returnUserHomes();

        this.populateFields();

        User user = new User();

        if (email != null) {
            user = this.services.getUserByEmailService(email);

            Home h = new Home();

            h.setCity(this.homeCity);
            h.setName(this.homeName);
            h.setStreet(this.homeStreet);
            h.setZipCode(this.homeZipCode);

            if (this.services.getHomeByNameService(homeName) != null) {
                this.returnUserHomes();
                this.homeName = null;
                FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_ERROR, "", "Ce nom de foyer a déjà été utilisé");
                FacesContext.getCurrentInstance().addMessage("newHome:newHomeName", msg);
                return;
            }

            if (this.services.getHomeByAddressService(homeStreet, homeZipCode, homeCity) != null) {
                this.returnUserHomes();
                this.homeCity = null;
                this.homeStreet = null;
                this.homeZipCode = null;
                FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Erreur", "Cette adresse a déjà été utilisée");
                FacesContext.getCurrentInstance().addMessage("newHome:newHomeMsg", msg);
                return;
            }

            if (this.homeZipCode.length() > 4) {
                this.homeZipCode = null;
                FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Erreur", "Le code postal doit avoir une longueur maximale de 4 caractères \"Ex: 2022\"");
                FacesContext.getCurrentInstance().addMessage("newHome:newHomeZipCode", msg);
                return;
            }

            this.services.createNewHomeService(h, user);
            this.returnUserHomes();
            this.returnMembersShip();
            FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_INFO, "Info", "Le nouveau foyer: " + this.homeName + " a été créé.");
            FacesContext.getCurrentInstance().addMessage("newHome:newHomeMsg", msg);

            this.refresh();
            RequestContext.getCurrentInstance().update("HomesCon:newHome homes");
        }
    }

    public void refresh() {
        this.homeCity = null;
        this.homeName = null;
        this.homeStreet = null;
        this.homeZipCode = null;
        this.returnMembersShip();

        try {
            if ((String) this.session.getAttribute("homeCity") != null) {
                this.session.setAttribute("homeCity", null);
            }
        } catch (Exception ex) {
        }

        try {
            if ((String) this.session.getAttribute("homeName") != null) {
                this.session.setAttribute("homeName", null);
            }
        } catch (Exception ex) {
        }

        try {
            if ((String) this.session.getAttribute("homeStreet") != null) {
                this.session.setAttribute("homeStreet", null);
            }
        } catch (Exception ex) {
        }

        try {
            if ((String) this.session.getAttribute("homeZipCode") != null) {
                this.session.setAttribute("homeZipCode", null);
            }
        } catch (Exception ex) {
        }

        RequestContext.getCurrentInstance().update("HomesCon:newHome homes");
    }

    public String homeVisual(String p_homeName) {
        HttpSession session = SessionUtils.getSession();
        session.setAttribute("homeName", p_homeName);
        return "success";
    }

    public boolean canBeDelete(String p_name) {
        Home h = this.services.getHomeByNameService(p_name);
        Membre m = this.services.getCurrentMemberShipByEmailNHomeService(this.email, p_name);
        UserStatusEnum us = m.getStatus();

        List<Membre> ms = new ArrayList<>();
        List<Membre> totMemList = new ArrayList<>();
        totMemList.addAll(this.services.getMembersByHomeService(h));
        totMemList.stream().filter((m1) -> (m1.getEndDate() == null)).forEachOrdered((m1) -> {
            ms.add(m1);
        });

        List<HomeRoom> homeRooms = new ArrayList<>();
        homeRooms.addAll(this.services.getHomeRoomsByHomeService(h));
        boolean isOk = false;

        if (homeRooms.isEmpty() && ms.size() < 2 && us.getCode().equalsIgnoreCase("SupAd")) {
            isOk = true;
        }

        return isOk;
    }

    public void delete(String p_name) {
        this.returnUserEmail();
        Home h = this.services.getHomeByNameService(p_name);
        services.deleteHomeService(h);
        this.returnUserHomes();
        this.returnMembersShip();
        FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_INFO, "", "Le foyer: " + p_name + " a été supprimé.");
        FacesContext.getCurrentInstance().addMessage("homes:homesMsg", msg);
    }

    //--------------------------------- Getter and Setter -----------------------------------------
    /**
     *
     * @return String
     */
    public String getHomeName() {
        return homeName;
    }

    public void setHomeName(String homeName) {
        this.homeName = homeName;
    }

    public String getHomeStreet() {
        return homeStreet;
    }

    public void setHomeStreet(String homeStreet) {
        this.homeStreet = homeStreet;
    }

    public String getHomeZipCode() {
        return homeZipCode;
    }

    public void setHomeZipCode(String homeZipCode) {
        this.homeZipCode = homeZipCode;
    }

    public String getHomeCity() {
        return homeCity;
    }

    public void setHomeCity(String homeCity) {
        this.homeCity = homeCity;
    }

    public List<Home> getHomes() {
        return homes;
    }

    public void setHomes(List<Home> homes) {
        this.homes = homes;
    }

    public List<Membre> getMembersShip() {
        this.returnMembersShip();
        return membersShip;
    }

    public void setMembersShip(List<Membre> membersShip) {
        this.membersShip = membersShip;
    }

}
