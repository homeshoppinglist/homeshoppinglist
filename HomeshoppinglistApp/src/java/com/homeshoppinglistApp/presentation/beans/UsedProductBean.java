package com.homeshoppinglistApp.presentation.beans;

import com.homeshoppinglistApp.business.HomeRoom;
import com.homeshoppinglistApp.business.HomeRoomProduct;
import com.homeshoppinglistApp.business.Product;
import com.homeshoppinglistApp.business.UnityQuantityEnum;
import com.homeshoppinglistApp.services.Service;
import com.homeshoppinglistApp.utilities.SessionUtils;
import java.io.Serializable;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.enterprise.context.RequestScoped;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.servlet.http.HttpSession;
import org.primefaces.context.RequestContext;

/**
 *
 * @author BorisK  <klettboris@gmailcom>
 */
@ManagedBean(name = "usedProdBean")
@RequestScoped
public class UsedProductBean implements Serializable {
    
    private static final long serialVersionUID = 1094801825228386363L;
    
    @Inject
    private Service services;
    private HttpSession session;
    private HomeRoom homeRoom;
    private String homeName;
    private String roomCode;
    private List<HomeRoomProduct> prodHRProductsList = new ArrayList<>();
    private String prodLimityQuantity;
    private String prodCurrentQuantity;
    private String prodPeremptionDate;
    private String prodCode;
    private String prodLibelle;
    private String prodUnityLibelle;
    private String prodUsedQuantity;
    private Date expDate;
    
    public UsedProductBean() {
    }
    
    @PostConstruct
    public void init() {
        this.postInit();
    }
    
    public void postInit() {
        try {
            session = SessionUtils.getSession();
            if (session != null) {
                this.homeName = (String) this.session.getAttribute("homeName");
                this.roomCode = (String) this.session.getAttribute("roomCode");
            }
        } catch (Exception ex) {
        }
        
        this.homeRoom = this.services.getHomeRoomByHNRService(this.services.getHomeByNameService(homeName),
                this.services.getRoomByCodeService(roomCode));
        
        this.prodHRProductsList = new ArrayList<>();
        this.prodHRProductsList.addAll(this.services.getHomeRoomProductsByRoomHomeService(this.homeRoom));
        if (this.prodHRProductsList.isEmpty()) {
            this.prodHRProductsList.addAll((Collection<? extends HomeRoomProduct>) this.session.getAttribute("prodHRProductsList"));
        }
    }
    
    public void useProdCloseModal() {
        this.reset();
        RequestContext.getCurrentInstance().execute("$('.modalPseudoClassProd').modal('hide')");
    }
    
    public void ravProdCloseModal() {
        this.reset();
        RequestContext.getCurrentInstance().execute("$('.modalPseudoClassProd2').modal('hide')");
    }
    
    public void useProdModal() {
        this.postInit();
        if (!this.prodHRProductsList.isEmpty()) {
            this.session.setAttribute("upNTrashButtonsDis", true);
            this.reset();
            RequestContext.getCurrentInstance().execute("$('.modalPseudoClassProd').modal()");
        }
    }
    
    public void ravProdModal() {
        this.postInit();
        if (!this.prodHRProductsList.isEmpty()) {
            this.session.setAttribute("upNTrashButtonsDis", true);
            this.reset();
            RequestContext.getCurrentInstance().execute("$('.modalPseudoClassProd2').modal()");
        }
    }
    
    public void useProduct() {
        this.getValues();
        if (this.checkValues()) {
            HomeRoomProduct hRP = this.services.getHomeRoomProductByHNPService(this.homeRoom,
                    this.services.getProductByLibelleService(this.prodLibelle));
            
            hRP.setCurrentQuantity(Integer.valueOf(this.prodCurrentQuantity) - Integer.valueOf(this.prodUsedQuantity));
            this.services.updateHomeRoomProductService(hRP);
            
            FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_INFO, "Info", "Le produit: " + this.prodLibelle + " a été utilisé");
            FacesContext.getCurrentInstance().addMessage("useProdForm:useProdMsg", msg);
            
            this.prodSelected();
            this.getValues();
            this.prodUsedQuantity = null;
        }
    }
    
    public void ravProduct() {
        this.getValues();
        if (this.checkValues4Rav()) {
            HomeRoomProduct hRP = this.services.getHomeRoomProductByHNPService(this.homeRoom,
                    this.services.getProductByLibelleService(this.prodLibelle));
            
            hRP.setCurrentQuantity(Integer.valueOf(this.prodCurrentQuantity) + Integer.valueOf(this.prodUsedQuantity));
            this.services.updateHomeRoomProductService(hRP);
            
            FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_INFO, "Info", "Le produit: " + this.prodLibelle + " a été ravitaillé");
            FacesContext.getCurrentInstance().addMessage("ravProdForm:ravProdMsg", msg);
            this.prodSelected();
            this.getValues();
            this.prodUsedQuantity = null;
        }
    }
    
    private boolean checkValues4Rav() {
        boolean isOk = true;
        try {
            if (this.prodLibelle.isEmpty() || this.prodLibelle.equalsIgnoreCase("") || this.prodLibelle.equalsIgnoreCase("Selectionner le produit")) {
                FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Erreur", "Veuillez choisir un produit");
                FacesContext.getCurrentInstance().addMessage("ravProdForm:prodName2", msg);
                isOk = false;
            }
        } catch (Exception ex) {
            FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Erreur", "Veuillez choisir un produit");
            FacesContext.getCurrentInstance().addMessage("ravProdForm:prodName2", msg);
            isOk = false;
        }
        
        try {
            if (this.prodCode.isEmpty() || this.prodCode.equalsIgnoreCase("")) {
                isOk = false;
            }
            if (this.prodPeremptionDate.isEmpty() || this.prodPeremptionDate.equalsIgnoreCase("")) {
                isOk = false;
            }
            if (this.prodLimityQuantity.isEmpty() || this.prodLimityQuantity.equalsIgnoreCase("")) {
                isOk = false;
            }
            if (this.prodCurrentQuantity.isEmpty() || this.prodCurrentQuantity.equalsIgnoreCase("")) {
                isOk = false;
            }
        } catch (Exception ex) {
            isOk = false;
        }
        
        try {
            if (this.prodUsedQuantity.isEmpty() || this.prodUsedQuantity.equalsIgnoreCase("")) {
                FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Erreur", "Veuillez saisir la quantité de ravitaillement");
                FacesContext.getCurrentInstance().addMessage("ravProdForm:ravQuantity", msg);
                isOk = false;
            }
        } catch (Exception ex) {
            FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Erreur", "Veuillez saisir la quantité de ravitaillement");
            FacesContext.getCurrentInstance().addMessage("ravProdForm:ravQuantity", msg);
            isOk = false;
        }
        
        try {
            if (isOk) {
                if (Integer.valueOf(this.prodUsedQuantity) < 0) {
                    FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Erreur", "Veuillez entrer une quantité souhaitée positive");
                    FacesContext.getCurrentInstance().addMessage("ravProdForm:ravQuantity", msg);
                    this.prodUsedQuantity = null;
                    this.getValues();
                    isOk = false;
                }
            } else {
                if (Integer.valueOf(this.prodUsedQuantity) < 0) {
                    this.prodUsedQuantity = null;
                    this.getValues();
                    isOk = false;
                }
            }
        } catch (Exception ex) {
            if (isOk) {
                FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Erreur", "Une Erreur s'est produite");
                FacesContext.getCurrentInstance().addMessage("ravProdForm:ravQuantity", msg);
                this.prodUsedQuantity = null;
                this.getValues();
            }
            this.prodUsedQuantity = null;
            this.getValues();
            isOk = false;
        }
        
        try {
            
            if (this.expDate != null) {
                Date date1 = Service.FORMATTER.parse(Service.FORMATTER.format(this.expDate));
                Date date2 = Service.FORMATTER.parse(Service.FORMATTER.format(new Date()));
                if (isOk) {
                    if (date1.compareTo(date2) < 0) {
                        FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_WARN, "Attention", "La date de peremption du produit est échue");
                        FacesContext.getCurrentInstance().addMessage("ravProdForm:ravProdMsg", msg);
                    }
                }
            }
            
        } catch (ParseException ex) {
            Logger.getLogger(ProductBean.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return isOk;
    }
    
    private void reset() {
        this.prodPeremptionDate = null;
        this.expDate = null;
        this.prodCurrentQuantity = null;
        this.prodLimityQuantity = null;
        this.prodCode = null;
        this.prodUnityLibelle = null;
        this.prodLibelle = "Selectionner le produit";
        this.prodUsedQuantity = null;
        
        this.session.setAttribute("prodPeremptionDate", null);
        this.session.setAttribute("expDate", null);
        this.session.setAttribute("prodCurrentQuantity", null);
        this.session.setAttribute("this.prodLimityQuantity", null);
        this.session.setAttribute("prodCode", null);
        this.session.setAttribute("prodUnityLibelle", null);
    }
    
    private boolean checkValues() {
        boolean isOk = true;
        try {
            if (this.prodLibelle.isEmpty() || this.prodLibelle.equalsIgnoreCase("") || this.prodLibelle.equalsIgnoreCase("Selectionner le produit")) {
                FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Erreur", "Veuillez choisir un produit");
                FacesContext.getCurrentInstance().addMessage("useProdForm:prodName", msg);
                isOk = false;
            }
        } catch (Exception ex) {
            FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Erreur", "Veuillez choisir un produit");
            FacesContext.getCurrentInstance().addMessage("useProdForm:prodName", msg);
            isOk = false;
        }
        
        try {
            if (this.prodCode.isEmpty() || this.prodCode.equalsIgnoreCase("")) {
                isOk = false;
            }
            if (this.prodPeremptionDate.isEmpty() || this.prodPeremptionDate.equalsIgnoreCase("")) {
                isOk = false;
            }
            if (this.prodLimityQuantity.isEmpty() || this.prodLimityQuantity.equalsIgnoreCase("")) {
                isOk = false;
            }
            if (this.prodCurrentQuantity.isEmpty() || this.prodCurrentQuantity.equalsIgnoreCase("")) {
                isOk = false;
            }
        } catch (Exception ex) {
            isOk = false;
        }
        
        try {
            if (this.prodUsedQuantity.isEmpty() || this.prodUsedQuantity.equalsIgnoreCase("")) {
                FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Erreur", "Veuillez saisir la quantité à utiliser");
                FacesContext.getCurrentInstance().addMessage("useProdForm:usedQuantity", msg);
                isOk = false;
            }
        } catch (Exception ex) {
            FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Erreur", "Veuillez saisir la quantité à utiliser");
            FacesContext.getCurrentInstance().addMessage("useProdForm:usedQuantity", msg);
            isOk = false;
        }
        
        try {
            if (isOk) {
                if (Integer.valueOf(this.prodUsedQuantity) < 0) {
                    FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Erreur", "Veuillez entrer une quantité souhaitée positive");
                    FacesContext.getCurrentInstance().addMessage("useProdForm:usedQuantity", msg);
                    this.prodUsedQuantity = null;
                    this.getValues();
                    isOk = false;
                } else if (Integer.valueOf(this.prodCurrentQuantity) == 0) {
                    FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Erreur", "Le produit est en rupture");
                    FacesContext.getCurrentInstance().addMessage("useProdForm:useProdMsg", msg);
                    this.prodUsedQuantity = null;
                    this.getValues();
                    isOk = false;
                } else if (Integer.valueOf(this.prodUsedQuantity) > Integer.valueOf(this.prodCurrentQuantity)) {
                    FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Erreur", "Pas assez de " + this.prodLibelle + " diponible");
                    FacesContext.getCurrentInstance().addMessage("useProdForm:usedQuantity", msg);
                    this.prodUsedQuantity = null;
                    this.getValues();
                    isOk = false;
                }
            } else {
                if (Integer.valueOf(this.prodUsedQuantity) < 0) {
                    this.prodUsedQuantity = null;
                    this.getValues();
                    isOk = false;
                } else if (Integer.valueOf(this.prodUsedQuantity) == 0) {
                    this.prodUsedQuantity = null;
                    this.getValues();
                    isOk = false;
                } else if (Integer.valueOf(this.prodUsedQuantity) > Integer.valueOf(this.prodCurrentQuantity)) {
                    this.prodUsedQuantity = null;
                    this.getValues();
                    isOk = false;
                }
            }
        } catch (Exception ex) {
            if (isOk) {
                FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Erreur", "Une Erreur s'est produite");
                FacesContext.getCurrentInstance().addMessage("useProdForm:usedQuantity", msg);
                this.prodUsedQuantity = null;
                this.getValues();
            }
            this.prodUsedQuantity = null;
            this.getValues();
            isOk = false;
        }
        
        try {
            if (Integer.valueOf(this.prodUsedQuantity) == Integer.valueOf(this.prodCurrentQuantity)) {
                FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_WARN, "Attention", "Le produit: " + this.prodLibelle + " est maintenant en rupture");
                FacesContext.getCurrentInstance().addMessage("useProdForm:usedQuantity", msg);
            }
        } catch (Exception ex) {
            if (isOk) {
                FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_WARN, "Attention", "Le produit: " + this.prodLibelle + " est maintenant en rupture");
                FacesContext.getCurrentInstance().addMessage("useProdForm:usedQuantity", msg);
            }
            isOk = false;
        }
        
        try {
            
            if (this.expDate != null) {
                Date date1 = Service.FORMATTER.parse(Service.FORMATTER.format(this.expDate));
                Date date2 = Service.FORMATTER.parse(Service.FORMATTER.format(new Date()));
                if (isOk) {
                    if (date1.compareTo(date2) < 0) {
                        FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_WARN, "Attention", "La date de peremption du produit est échue");
                        FacesContext.getCurrentInstance().addMessage("useProdForm:useProdMsg", msg);
                    }
                }
            }
            
        } catch (ParseException ex) {
            Logger.getLogger(ProductBean.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return isOk;
    }
    
    public void prodSelected() {
        if (this.prodLibelle.equalsIgnoreCase("Selectionner le produit")) {
            return;
        }
        HomeRoomProduct prodHRProductSelected = this.services.getHomeRoomProductByHNPService(this.homeRoom,
                this.services.getProductByLibelleService(this.prodLibelle));
        this.prodPeremptionDate = Service.FORMATTER.format(prodHRProductSelected.getPeremptionDate());
        this.expDate = prodHRProductSelected.getPeremptionDate();
        this.prodCurrentQuantity = prodHRProductSelected.getCurrentQuantity().toString();
        this.prodLimityQuantity = prodHRProductSelected.getLimitQuantity().toString();
        
        Product prod = prodHRProductSelected.getProduct();
        this.prodCode = prod.getCode();
        UnityQuantityEnum unity = prod.getUnity();
        this.prodUnityLibelle = unity.getDescription();
        
        this.session.setAttribute("prodPeremptionDate", this.prodPeremptionDate);
        this.session.setAttribute("expDate", this.expDate);
        this.session.setAttribute("prodCurrentQuantity", this.prodCurrentQuantity);
        this.session.setAttribute("this.prodLimityQuantity", this.prodLimityQuantity);
        this.session.setAttribute("prodCode", this.prodCode);
        this.session.setAttribute("prodUnityLibelle", this.prodUnityLibelle);
        
        RequestContext.getCurrentInstance().update("useProdForm");
        RequestContext.getCurrentInstance().update("ravProdForm");
    }
    
    private void getValues() {
        try {
            if (!this.prodPeremptionDate.isEmpty() || !this.prodPeremptionDate.equalsIgnoreCase("")) {
                this.session.setAttribute("prodPeremptionDate", this.prodPeremptionDate);
            }
        } catch (Exception ex) {
        }
        
        try {
            if (this.expDate != null) {
                this.session.setAttribute("expDate", this.expDate);
            }
        } catch (Exception ex) {
        }
        
        try {
            if (!this.prodCurrentQuantity.isEmpty() || !this.prodCurrentQuantity.equalsIgnoreCase("")) {
                this.session.setAttribute("prodCurrentQuantity", this.prodCurrentQuantity);
            }
        } catch (Exception ex) {
        }
        
        try {
            if (!this.prodLimityQuantity.isEmpty() || !this.prodLimityQuantity.equalsIgnoreCase("")) {
                this.session.setAttribute("this.prodLimityQuantity", this.prodLimityQuantity);
            }
        } catch (Exception ex) {
        }
        
        try {
            if (!this.prodCode.isEmpty() || !this.prodCode.equalsIgnoreCase("")) {
                this.session.setAttribute("prodCode", this.prodCode);
            }
        } catch (Exception ex) {
        }
        
        try {
            if (!this.prodUnityLibelle.isEmpty() || !this.prodUnityLibelle.equalsIgnoreCase("")) {
                this.session.setAttribute("prodUnityLibelle", this.prodUnityLibelle);
            }
        } catch (Exception ex) {
        }
        
        try {
            if (this.prodPeremptionDate.isEmpty() || this.prodPeremptionDate.equalsIgnoreCase("")) {
                this.prodPeremptionDate = (String) this.session.getAttribute("prodPeremptionDate");
            }
        } catch (Exception ex) {
            this.prodPeremptionDate = (String) this.session.getAttribute("prodPeremptionDate");
        }
        
        try {
            if (this.expDate == null) {
                this.expDate = (Date) this.session.getAttribute("expDate");
            }
        } catch (Exception ex) {
            this.expDate = (Date) this.session.getAttribute("expDate");
        }
        
        try {
            if (this.prodCurrentQuantity.isEmpty() || this.prodCurrentQuantity.equalsIgnoreCase("")) {
                this.prodCurrentQuantity = (String) this.session.getAttribute("prodCurrentQuantity");
            }
        } catch (Exception ex) {
            this.prodCurrentQuantity = (String) this.session.getAttribute("prodCurrentQuantity");
        }
        
        try {
            if (this.prodLimityQuantity.isEmpty() || this.prodLimityQuantity.equalsIgnoreCase("")) {
                this.prodLimityQuantity = (String) this.session.getAttribute("this.prodLimityQuantity");
            }
        } catch (Exception ex) {
            this.prodLimityQuantity = (String) this.session.getAttribute("this.prodLimityQuantity");
        }
        
        try {
            if (this.prodCode.isEmpty() || this.prodCode.equalsIgnoreCase("")) {
                this.prodCode = (String) this.session.getAttribute("prodCode");
            }
        } catch (Exception ex) {
            this.prodCode = (String) this.session.getAttribute("prodCode");
        }
        
        try {
            if (this.prodUnityLibelle.isEmpty() || this.prodUnityLibelle.equalsIgnoreCase("")) {
                this.prodUnityLibelle = (String) this.session.getAttribute("prodUnityLibelle");
            }
        } catch (Exception ex) {
            this.prodUnityLibelle = (String) this.session.getAttribute("prodUnityLibelle");
        }
        
    }
    
    public String getProdUsedQuantity() {
        return prodUsedQuantity;
    }
    
    public void setProdUsedQuantity(String prodUsedQuantity) {
        this.prodUsedQuantity = prodUsedQuantity;
    }
    
    public String getProdUnityLibelle() {
        return prodUnityLibelle;
    }
    
    public void setProdUnityLibelle(String prodUnityLibelle) {
        this.prodUnityLibelle = prodUnityLibelle;
    }
    
    public String getProdLibelle() {
        return prodLibelle;
    }
    
    public void setProdLibelle(String prodLibelle) {
        this.prodLibelle = prodLibelle;
    }
    
    public HomeRoom getHomeRoom() {
        return homeRoom;
    }
    
    public void setHomeRoom(HomeRoom homeRoom) {
        this.homeRoom = homeRoom;
    }
    
    public String getHomeName() {
        return homeName;
    }
    
    public void setHomeName(String homeName) {
        this.homeName = homeName;
    }
    
    public String getRoomCode() {
        return roomCode;
    }
    
    public void setRoomCode(String roomCode) {
        this.roomCode = roomCode;
    }
    
    public List<HomeRoomProduct> getProdHRProductsList() {
        return prodHRProductsList;
    }
    
    public void setProdHRProductsList(List<HomeRoomProduct> prodHRProductsList) {
        this.prodHRProductsList = prodHRProductsList;
    }
    
    public String getProdLimityQuantity() {
        return prodLimityQuantity;
    }
    
    public void setProdLimityQuantity(String prodLimityQuantity) {
        this.prodLimityQuantity = prodLimityQuantity;
    }
    
    public String getProdCurrentQuantity() {
        return prodCurrentQuantity;
    }
    
    public void setProdCurrentQuantity(String prodCurrentQuantity) {
        this.prodCurrentQuantity = prodCurrentQuantity;
    }
    
    public String getProdPeremptionDate() {
        return prodPeremptionDate;
    }
    
    public void setProdPeremptionDate(String prodPeremptionDate) {
        this.prodPeremptionDate = prodPeremptionDate;
    }
    
    public String getProdCode() {
        return prodCode;
    }
    
    public void setProdCode(String prodCode) {
        this.prodCode = prodCode;
    }
    
}
