package com.homeshoppinglistApp.presentation.beans;

import com.homeshoppinglistApp.business.User;
import com.homeshoppinglistApp.services.Service;
import com.homeshoppinglistApp.utilities.SessionUtils;
import java.io.Serializable;
import javax.annotation.PostConstruct;
import javax.enterprise.context.SessionScoped;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.validation.constraints.Size;
import javax.servlet.http.HttpSession;

/**
 *
 * @author BorisK <klettboris@gmail.com>
 */
@ManagedBean(name = "loginBean")
@SessionScoped
public class LoginBean implements Serializable {

    private static final long serialVersionUID = 1094801825228386363L;

    @Inject
    private Service services;

    @Size(min = 0, max = 20)
    private User user;
    private String userPwd;
    private String userEmail;
    private boolean connexionFieldRendered = true;
    private boolean newPasswordRendered = false;
    private boolean isShaking = false;
    private String lookFieldset = "primary";

    public LoginBean() {
        try {
            connexionFieldRendered = true;
            newPasswordRendered = false;
            isShaking = false;
            lookFieldset = "primary";

            HttpSession session = SessionUtils.getSession();
            this.user = this.services.getUserByEmailService((String) session.getAttribute("userEmail"));
        } catch (Exception ex) {
        }
    }

    @PostConstruct
    public void init() {
        connexionFieldRendered = true;
        newPasswordRendered = false;
        isShaking = false;
        lookFieldset = "primary";

    }

    public User getUser() {
        HttpSession session = SessionUtils.getSession();
        this.user = this.services.getUserByEmailService((String) session.getAttribute("userEmail"));
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public String getLookFieldset() {
        return lookFieldset;
    }

    public void setLookFieldset(String lookFieldset) {
        this.lookFieldset = lookFieldset;
    }

    public boolean isConnexionFieldRendered() {
        return connexionFieldRendered;
    }

    public boolean isIsShaking() {
        return isShaking;
    }

    public void setIsShaking(boolean isShaking) {
        this.isShaking = isShaking;
    }

    public void setConnexionFieldRendered(boolean connexionFieldRendered) {
        this.connexionFieldRendered = connexionFieldRendered;
    }

    public boolean isNewPasswordRendered() {
        return newPasswordRendered;
    }

    public void setNewPasswordRendered(boolean newPasswordRendered) {
        this.newPasswordRendered = newPasswordRendered;
    }

    public String getUserPwd() {
        return userPwd;
    }

    public void setUserPwd(String userPwd) {
        this.userPwd = userPwd;
    }

    public String getUserEmail() {
        return userEmail;
    }

    public void setUserEmail(String userEmail) {
        this.userEmail = userEmail;
    }

    //validate login
    public String validateUsernamePassword() {

        try {
            if (userEmail.isEmpty()) {
                FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_ERROR, "", "Veuillez entrer votre email");
                isShaking = false;
                lookFieldset = "primary";
                FacesContext.getCurrentInstance().addMessage("connexionTabView:logingTab:loginForm:username", msg);
                return "error";
            } else if (userPwd.isEmpty()) {
                FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_ERROR, "", "Veuillez entre votre mot de passe");
                isShaking = false;
                lookFieldset = "primary";
                FacesContext.getCurrentInstance().addMessage("connexionTabView:logingTab:loginForm:password", msg);
                return "error";
            } else {
                boolean valid = services.userConnectService(userEmail, userPwd);

                if (valid) {
                    HttpSession session = SessionUtils.getSession();
                    session.setAttribute("userEmail", this.userEmail);
                    lookFieldset = "success";
                    FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_INFO, "", "Vous vous êtes connecté avec succès!");
                    FacesContext.getCurrentInstance().addMessage("connexionTabView:logingTab:loginForm:loginMsg", msg);
                    this.user = this.services.getUserByEmailService(userEmail);
                    return "success";

                } else {
                    isShaking = true;
                    lookFieldset = "danger";
                    FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_ERROR, "", "La connexion a échouée!");
                    FacesContext.getCurrentInstance().addMessage("connexionTabView:logingTab:loginForm:loginMsgRow:loginMsg", msg);
                    return "error";
                }
            }
        } catch (Exception ex) {
            FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_ERROR, "", "Veuillez saisir correctement votre email et votre mot de passe!");
            isShaking = true;
            lookFieldset = "danger";
            FacesContext.getCurrentInstance().addMessage(":loginMsgRow:loginMsg", msg);
            return "error";
        }

    }

    public String logout() {
        HttpSession session = SessionUtils.getSession();
        session.setAttribute("userEmail", null);
        session.setAttribute("userId", null);
        session.setAttribute("user", null);
        session.setAttribute("homeName", null);
        session.setAttribute("memberId", null);
        session.invalidate();
        return "success";

    }

    public void forgotPassword() {
        connexionFieldRendered = false;
        newPasswordRendered = true;
    }

    public void returnToConnexion() {
        connexionFieldRendered = true;
        newPasswordRendered = false;
    }
}
