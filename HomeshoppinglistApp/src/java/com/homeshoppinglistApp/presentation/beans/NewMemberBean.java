package com.homeshoppinglistApp.presentation.beans;

import com.homeshoppinglistApp.business.Home;
import com.homeshoppinglistApp.business.Membre;
import com.homeshoppinglistApp.business.User;
import com.homeshoppinglistApp.business.UserStatusEnum;
import com.homeshoppinglistApp.services.Service;
import com.homeshoppinglistApp.utilities.SessionUtils;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.enterprise.context.RequestScoped;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.servlet.http.HttpSession;

/**
 *
 * @author BorisK<klettboris@gmail.com>
 */
@ManagedBean(name = "newMemberBean")
@RequestScoped
public class NewMemberBean implements Serializable {

    private static final long serialVersionUID = 1094801825228386363L;

    @Inject
    private Service services;

    private HttpSession session;
    private Home home;
    private String newMembEmail;
    private String newMembLastname;
    private String newMembFirstname;
    private String newMembStatusCode;
    private List<UserStatusEnum> newMembStatusList = new ArrayList<>();

    public NewMemberBean() {
    }

    @PostConstruct
    public void init() {
        this.postInit();
    }

    public void postInit() {
        try {
            session = SessionUtils.getSession();
            this.home = new Home();
            this.home = this.services.getHomeByNameService((String) session.getAttribute("homeName"));
        } catch (Exception ex) {
        }

        List<UserStatusEnum> nmsl = new ArrayList<>();
        this.newMembStatusList = new ArrayList<>();
        nmsl.addAll(this.services.getStatusList());
        Membre m = this.services.getCurrentMemberShipByEmailNHomeService((String) this.session.getAttribute("userEmail"), this.home.getName());

        nmsl.forEach((us) -> {
            if (us.getCode().equalsIgnoreCase("SupAd") && !m.getStatus().getCode().equalsIgnoreCase("SupAd")) {
                //No thing to do
            } else if (us.getCode().equalsIgnoreCase("SupAd") && m.getStatus().getCode().equalsIgnoreCase("SupAd")) {
                this.newMembStatusList.add(us);
            } else {
                this.newMembStatusList.add(us);
            }
        });

    }

    public void findUser() {

        try {
            User u = this.services.getUserByEmailService(this.newMembEmail);
            this.session.setAttribute("email", this.newMembEmail);

            if (u != null) {
                this.newMembFirstname = u.getFirstname();
                this.newMembLastname = u.getLastname();
                this.newMembEmail = u.getEmail();
                this.session.setAttribute("fstname", this.newMembFirstname);
                this.session.setAttribute("lstname", this.newMembLastname);

            } else {
                FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_ERROR, "", "Aucun utilisateur n'utilise l'adresse email: " + this.newMembEmail);
                FacesContext.getCurrentInstance().addMessage("myFormID:newMembMsg", msg);
            }
        } catch (NullPointerException ex) {
            FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_ERROR, "", "Aucun utilisateur n'utilise l'adresse email: " + this.newMembEmail);
            FacesContext.getCurrentInstance().addMessage("myFormID:newMembMsg", msg);
        }
    }

    public void newMemb() {
        FacesMessage msg;
        this.postInit();
        this.session.setAttribute("email", this.newMembEmail);
        this.newMembEmail = (String) this.session.getAttribute("email");
        Membre m = this.services.getCurrentMemberShipByEmailNHomeService(this.newMembEmail, this.home.getName());
        this.newMembFirstname = (String) this.session.getAttribute("fstname");
        this.newMembLastname = (String) this.session.getAttribute("lstname");

        if (m != null) {
            this.newMembFirstname = m.getUser().getFirstname();
            msg = new FacesMessage(FacesMessage.SEVERITY_ERROR, "", this.newMembFirstname + " est déjà membre du foyer: " + this.home.getName());
            FacesContext.getCurrentInstance().addMessage("myFormID:newMembMsg", msg);
            this.newMembFirstname = null;
            this.session.setAttribute("fstname", null);
            this.newMembLastname = null;
            this.session.setAttribute("lstname", null);
            return;
        }

        try {
            if (this.newMembEmail.isEmpty() || this.newMembEmail == null) {
                msg = new FacesMessage(FacesMessage.SEVERITY_ERROR, "", "Veuillez saisir une adresse email valable");
                FacesContext.getCurrentInstance().addMessage("myFormID:newMembMsg", msg);
                return;
            }

            if (this.newMembFirstname.isEmpty() || this.newMembFirstname == null) {
                msg = new FacesMessage(FacesMessage.SEVERITY_ERROR, "", "Veuillez saisir une adresse email valable");
                FacesContext.getCurrentInstance().addMessage("myFormID:newMembMsg", msg);
                return;
            }
        } catch (Exception ex) {
            msg = new FacesMessage(FacesMessage.SEVERITY_ERROR, "", "Veuillez saisir une adresse email valable et lancer la recherche de l'utilisateur");
            FacesContext.getCurrentInstance().addMessage("myFormID:newMembMsg", msg);
            return;
        }

        if (this.newMembStatusCode.equalsIgnoreCase("-- SelectOne --")) {
            msg = new FacesMessage(FacesMessage.SEVERITY_ERROR, "", "Veuillez choisir le statut du nouveau membre");
            FacesContext.getCurrentInstance().addMessage("myFormID:newMembMsg", msg);
            return;
        }

        if (!this.services.getOldMembersShipListByEmailNHomeService(newMembEmail, this.home.getName()).isEmpty()) {
            User u = this.services.getUserByEmailService(this.newMembEmail);
            this.services.addHomeMemberService(u, home, this.services.getStatusByCodeService(this.newMembStatusCode));

            msg = new FacesMessage(FacesMessage.SEVERITY_INFO, "", this.newMembFirstname + " est à nouveau membre du foyer: " + this.home.getName());
            FacesContext.getCurrentInstance().addMessage("myFormID:newMembMsg", msg);
            this.newMembFirstname = null;
            this.session.setAttribute("fstname", null);
            this.newMembLastname = null;
            this.session.setAttribute("lstname", null);
            this.session.setAttribute("email", null);
            this.newMembEmail = null;
            this.newMembStatusCode = "-- SelectOne --";
            this.postInit();
            return;
        }

        this.services.addHomeMemberService(this.services.getUserByEmailService(this.newMembEmail), home, this.services.getStatusByCodeService(this.newMembStatusCode));
        msg = new FacesMessage(FacesMessage.SEVERITY_INFO, "", this.newMembFirstname + " est un nouveau membre du foyer: " + this.home.getName());
        FacesContext.getCurrentInstance().addMessage("myFormID:newMembMsg", msg);
        this.newMembFirstname = null;
        this.session.setAttribute("fstname", null);
        this.newMembLastname = null;
        this.session.setAttribute("lstname", null);
        this.session.setAttribute("email", null);
        this.newMembEmail = null;
        this.newMembStatusCode = "-- SelectOne --";
        this.postInit();
    }

    public String getNewMembEmail() {
        return newMembEmail;
    }

    public void setNewMembEmail(String newMembEmail) {
        this.newMembEmail = newMembEmail;
    }

    public String getNewMembLastname() {
        return newMembLastname;
    }

    public void setNewMembLastname(String newMembLastname) {
        this.newMembLastname = newMembLastname;
    }

    public String getNewMembFirstname() {
        return newMembFirstname;
    }

    public void setNewMembFirstname(String newMembFirstname) {
        this.newMembFirstname = newMembFirstname;
    }

    public String getNewMembStatusCode() {
        return newMembStatusCode;
    }

    public void setNewMembStatusCode(String newMembStatusCode) {
        this.newMembStatusCode = newMembStatusCode;
    }

    public List<UserStatusEnum> getNewMembStatusList() {
        return newMembStatusList;
    }

    public void setNewMembStatusList(List<UserStatusEnum> newMembStatusList) {
        this.newMembStatusList = newMembStatusList;
    }

}
