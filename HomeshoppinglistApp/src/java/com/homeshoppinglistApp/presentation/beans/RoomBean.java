package com.homeshoppinglistApp.presentation.beans;

import com.homeshoppinglistApp.business.RoomEnum;
import com.homeshoppinglistApp.services.Service;
import com.homeshoppinglistApp.utilities.SessionUtils;
import java.io.Serializable;
import javax.annotation.PostConstruct;
import javax.enterprise.context.RequestScoped;
import javax.faces.bean.ManagedBean;
import javax.inject.Inject;
import javax.servlet.http.HttpSession;

/**
 *
 * @author BorisK <klettboris@gmail.com>
 */
@ManagedBean(name = "roomBean")
@RequestScoped

public class RoomBean implements Serializable {

    private static final long serialVersionUID = 1094801825228386363L;

    @Inject
    private Service services;

    private HttpSession session;

    private String roomCode;
    private String roomLibelle;

    public RoomBean() {
    }

    @PostConstruct
    public void init() {
        try {
            session = SessionUtils.getSession();
            if (session != null) {
                this.roomCode = (String) this.session.getAttribute("roomCode");
                RoomEnum room = this.services.getRoomByCodeService(roomCode);
                this.roomLibelle = room.getDescription();
            }
        } catch (Exception ex) {
        }

    }

    public String getRoomCode() {
        return roomCode;
    }

    public void setRoomCode(String roomCode) {
        this.roomCode = roomCode;
    }

    public String getRoomLibelle() {
        return roomLibelle;
    }

    public void setRoomLibelle(String roomLibelle) {
        this.roomLibelle = roomLibelle;
    }

}
