package com.homeshoppinglistApp.services;

import com.homeshoppinglistApp.business.Home;
import com.homeshoppinglistApp.business.HomeRoom;
import com.homeshoppinglistApp.business.HomeRoomProduct;
import com.homeshoppinglistApp.business.Membre;
import com.homeshoppinglistApp.business.Product;
import com.homeshoppinglistApp.business.RoomEnum;
import com.homeshoppinglistApp.business.UnityQuantityEnum;
import com.homeshoppinglistApp.business.User;
import com.homeshoppinglistApp.business.UserStatusEnum;
import com.homeshoppinglistApp.persistence.HomePersistence;
import com.homeshoppinglistApp.persistence.HomeRoomPersistence;
import com.homeshoppinglistApp.persistence.HomeRoomProductPersistence;
import com.homeshoppinglistApp.persistence.MemberPersistence;
import com.homeshoppinglistApp.persistence.ProductPersistence;
import com.homeshoppinglistApp.persistence.RoomPersistence;
import com.homeshoppinglistApp.persistence.UnityQuantityPersistence;
import com.homeshoppinglistApp.persistence.UserPersistence;
import com.homeshoppinglistApp.persistence.UserStatusPersistence;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import javax.ejb.Stateless;
import javax.inject.Inject;

/**
 * This is the services class
 *
 * @author BorisK <klettboris@gmail.com>
 */
@Stateless
public class Service implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     *
     */
    public static final SimpleDateFormat FORMATTER_FULL_YEAR_FORMAT = new SimpleDateFormat("dd.MM.yyyy");

    /**
     *
     */
    public static final SimpleDateFormat FORMATTER = new SimpleDateFormat("dd.MM.yy");

    /**
     *
     */
    public static final SimpleDateFormat H = new SimpleDateFormat("mm");

    @Inject
    private UserPersistence userPersistence;

    @Inject
    private UserStatusPersistence userStatusPersistence;

    @Inject
    private HomePersistence homePersistence;

    @Inject
    private HomeRoomPersistence homeRoomPersistence;

    @Inject
    private HomeRoomProductPersistence homeRoomProductPersistence;

    @Inject
    private MemberPersistence memberPersistence;

    @Inject
    private ProductPersistence productPersistence;

    @Inject
    private RoomPersistence roomPersistence;

    @Inject
    private UnityQuantityPersistence unityQuantityPersistence;

    /**
     * this method allow to get an user by his/her id
     *
     * @param p_id
     * @return User
     */
    public User getUserByIdService(Integer p_id) {
        return this.userPersistence.getUserById(p_id);
    }//Ok

    /**
     * Allow to get an user by his/her email address
     *
     * @param p_email
     * @return User
     */
    public User getUserByEmailService(String p_email) {
        return this.userPersistence.getUserByEmail(p_email);
    }//Ok

    /**
     * Allow to get an user by his/her firstname, lastname and birthdate
     *
     * @param p_firstname
     * @param p_lastname
     * @param p_birthdate
     * @return
     */
    public User getUserByInfosService(String p_firstname, String p_lastname, Date p_birthdate) {
        return this.userPersistence.getUserByInfos(p_firstname, p_lastname, p_birthdate);
    }//Ok

    /**
     * Allow to create a new user
     *
     * @param p_user
     * @return User
     */
    public User createNewUserService(User p_user) {
        this.userPersistence.createNewUser(p_user);
        return this.userPersistence.getUserByEmail(p_user.getEmail());
    }//Ok

    /**
     * Allow to update an user
     *
     * @param p_user
     * @return User
     */
    public User updateUserService(User p_user) {
        User u = this.userPersistence.getUserByEmail(p_user.getEmail());
        u.setBirthdate(p_user.getBirthdate());
        u.setEmail(p_user.getEmail());
        u.setFirstname(p_user.getFirstname());
        u.setLastname(p_user.getLastname());
        u.setPassword(p_user.getPassword());

        this.userPersistence.updateUser(u);
        u = this.userPersistence.getUserById(u.getId());
        return u;
    }//Ok

    /**
     * Allow to delete an user
     *
     * @param p_user
     */
    public void deleteUserService(User p_user) {
        User u = this.userPersistence.getUserByEmail(p_user.getEmail());
        this.userPersistence.deleteUser(u);
    }//Ok

    /**
     * Allow to get a list of users
     *
     * @return List<User>
     */
    public List<User> getUserListService() {
        return this.userPersistence.getUserList();
    }//Ok

    /**
     * Allow to connect to the app
     *
     * @param p_userEmail
     * @param p_userPwd
     * @return boolean
     */
    public boolean userConnectService(String p_userEmail, String p_userPwd) {
        if (this.userPersistence.userConnect(p_userEmail, p_userPwd) == null) {
            return false;
        }
        return true;
    }//New//Ok

    /**
     * Allow to get member status by code
     *
     * @param p_code
     * @return UserStatusEnum
     */
    public UserStatusEnum getStatusByCodeService(String p_code) {
        return this.userStatusPersistence.getStatusByCode(p_code);
    }//Ok

    /**
     * Allow to get member status list
     *
     * @return List<UserStatusEnum>
     */
    public List<UserStatusEnum> getStatusList() {
        return this.userStatusPersistence.getUserSatusList();
    }//Ok

    /**
     * Allow to get member status by id
     *
     * @param p_id
     * @return UserStatusEnum
     */
    public UserStatusEnum getUserStatusByIdService(Integer p_id) {
        return this.userStatusPersistence.getUserStatusById(p_id);
    }//Ok

    /**
     * Allow to get a member by id
     *
     * @param p_id
     * @return Membre
     */
    public Membre getMemberByIdService(Integer p_id) {
        return this.memberPersistence.getMemberById(p_id);
    }//Ok

    /**
     * Allow to get a member by his/her email address and home
     *
     * @param p_email
     * @param p_homeName
     * @return Membre
     */
    public Membre getMemberByEmailNHomeService(String p_email, String p_homeName) {
        return this.memberPersistence.getMemberByUserNHome(this.userPersistence.getUserByEmail(p_email),
                this.homePersistence.getHomeByName(p_homeName));
    }//Ok

    /**
     * Allow to get a member by his/her email address and home
     *
     * @param p_email
     * @param p_homeName
     * @return Membre
     */
    public Membre getCurrentMemberShipByEmailNHomeService(String p_email, String p_homeName) {
        return this.memberPersistence.getCurrentMemberShipByUserNHome(this.userPersistence.getUserByEmail(p_email),
                this.homePersistence.getHomeByName(p_homeName));
    }//Ok

    /**
     * Allow to get a member by his/her email address and home
     *
     * @param p_email
     * @param p_homeName
     * @return List<Membre>
     */
    public List<Membre> getMembersListByEmailNHomeService(String p_email, String p_homeName) {
        return this.memberPersistence.getMembersListByUserNHome(this.userPersistence.getUserByEmail(p_email),
                this.homePersistence.getHomeByName(p_homeName));
    }//Ok

    /**
     *
     * @param p_email
     * @return List<Membre>
     */
    public List<Membre> getCurrentMembersListByEmailService(String p_email) {
        return this.memberPersistence.getCurrentMembersShipListByUser(this.userPersistence.getUserByEmail(p_email));
    }//Ok

    /**
     * Allow to get a member by his/her email address and home
     *
     * @param p_email
     * @param p_homeName
     * @return List<Membre>
     */
    public List<Membre> getOldMembersShipListByEmailNHomeService(String p_email, String p_homeName) {
        return this.memberPersistence.getOldMembersShipListByUserNHome(this.userPersistence.getUserByEmail(p_email),
                this.homePersistence.getHomeByName(p_homeName));
    }//Ok

    /**
     * Add a new member to home
     *
     * @param p_user
     * @param p_home
     * @return Membre
     */
    public Membre addHomeMemberWMembStatService(User p_user, Home p_home) {
        User u = this.userPersistence.getUserByEmail(p_user.getEmail());
        Home h = this.homePersistence.getHomeByName(p_home.getName());

        this.memberPersistence.addHomeMember(u, h, this.userStatusPersistence.getStatusByCode("Memb"));

        Membre m = this.memberPersistence.getMemberByUserNHome(u, h);
        return m;
    }//Ok

    /**
     *
     * @param p_user
     * @param p_home
     * @param p_stat
     * @return Membre
     */
    public Membre addHomeMemberService(User p_user, Home p_home, UserStatusEnum p_stat) {
        User u = this.userPersistence.getUserByEmail(p_user.getEmail());
        Home h = this.homePersistence.getHomeByName(p_home.getName());

        this.memberPersistence.addHomeMember(u, h, p_stat);

        Membre m = this.memberPersistence.getMemberByUserNHome(u, h);
        return m;
    }//Ok

    /**
     * Delete a member from home
     *
     * @param p_member
     * @param p_date
     */
    public void deleteMemberService(Membre p_member, Date p_date) {
        Membre m = this.memberPersistence.getMemberByUserNHome(p_member.getUser(), p_member.getHome());
        this.memberPersistence.deleteMember(m, p_date);
    }//Ok

    /**
     * Update a member
     *
     * @param p_member
     * @return Membre
     */
    public Membre updateMemberService(Membre p_member) {
        Membre m;
        this.memberPersistence.updateMember(p_member);
        m = this.memberPersistence.getMemberById(p_member.getId());
        return m;
    }//Ok

    /**
     * Get a list of members by home
     *
     * @param p_home
     * @return List<Membre>
     */
    public List<Membre> getMembersByHomeService(Home p_home) {
        return this.memberPersistence.getMembersByHome(this.homePersistence.getHomeByName(p_home.getName()));
    }//Ok

    /**
     * Get a list of members
     *
     * @return List<Membre>
     */
    public List<Membre> getMembersListService() {
        return this.memberPersistence.getMembersList();
    }//Ok

    /**
     * Add an administrator member to a home
     *
     * @param p_user
     * @param p_home
     * @return Membre
     */
    public Membre createAminHomeMemberService(User p_user, Home p_home) {
        Membre m = this.memberPersistence.addHomeMember(p_user, p_home, this.userStatusPersistence.getStatusByCode("Admin"));
        return m;
    }//Ok 

    /**
     * Add a super administrator member to a home
     *
     * @param p_user
     * @param p_home
     * @return Membre
     */
    public Membre createSuperAminHomeMemberService(User p_user, Home p_home) {
        Membre m = this.memberPersistence.addHomeMember(p_user, p_home, this.userStatusPersistence.getStatusByCode("SupAd"));
        return m;
    }//Ok 

    /**
     * get home by name
     *
     * @param p_homeName
     * @return Home
     */
    public Home getHomeByNameService(String p_homeName) {
        return this.homePersistence.getHomeByName(p_homeName);
    }//Ok

    /**
     * Get home by address
     *
     * @param p_street
     * @param p_zipCode
     * @param p_city
     * @return Home
     */
    public Home getHomeByAddressService(String p_street, String p_zipCode, String p_city) {
        return this.homePersistence.getHomeByAddress(p_street, p_zipCode, p_city);
    }//Ok

    /**
     * Create new home
     *
     * @param p_user
     * @param p_home
     * @return Home
     */
    public Home createNewHomeService(Home p_home, User p_user) {

        Home h = null;
        this.homePersistence.createNewHome(p_home);
        try {
            h = this.homePersistence.getHomeByName(p_home.getName());
            if (h != null) {
                this.createSuperAminHomeMemberService(p_user, h);
            }
        } catch (Exception ex) {
        }
        return h;
    }//Ok

    /**
     * Delete home
     *
     * @param p_home
     */
    public void deleteHomeService(Home p_home) {
        Home h = this.homePersistence.getHomeByName(p_home.getName());
        this.homePersistence.deleteHome(h);
    }//Ok

    /**
     * Update home
     *
     * @param p_home
     * @return Home
     */
    public Home updateHomeService(Home p_home) {
        this.homePersistence.updateHome(p_home);
        Home h = this.homePersistence.getHomeByName(p_home.getName());
        return h;
    }//Ok

    /**
     *
     * @param p_homeRoom
     * @return boolean
     */
    public boolean isRoomHomeExistService(HomeRoom p_homeRoom) {
        return this.homeRoomPersistence.isRoomHomeExist(p_homeRoom);
    }//Ok

    /**
     * Add a room to home
     *
     * @param p_homeRoom
     * @return HomeRoom
     */
    public HomeRoom addHomeRoomService(HomeRoom p_homeRoom) {
        this.homeRoomPersistence.addRoomHome(p_homeRoom);
        return this.homeRoomPersistence.getHRByHomeNRoom(p_homeRoom.getHome(), p_homeRoom.getRoom());
    }//Ok

    /**
     * Delete a room from home
     *
     * @param p_homeRoom
     */
    public void deleteHomeRoomService(HomeRoom p_homeRoom) {
        this.homeRoomPersistence.deleteRoomHome(this.homeRoomPersistence.getHRByHomeNRoom(p_homeRoom.getHome(),
                p_homeRoom.getRoom()));
    }//Ok

    /**
     * Get HomeRoomsList by home
     *
     * @param p_home
     * @return List<HomeRoom>
     */
    public List<HomeRoom> getHomeRoomsByHomeService(Home p_home) {
        return this.homeRoomPersistence.getRoomsByHome(this.homePersistence.getHomeByName(p_home.getName()));
    }//Ok

    /**
     * Get HomeRoom by Home and Room
     *
     * @param p_home
     * @param p_room
     * @return HomeRoom
     */
    public HomeRoom getHomeRoomByHNRService(Home p_home, RoomEnum p_room) {
        HomeRoom h = this.homeRoomPersistence.getHRByHomeNRoom(p_home, p_room);
        return h;
    }//Ok

    /**
     * Get a room by id
     *
     * @param p_id
     * @return RoomEnum
     */
    public RoomEnum getRoomByIdService(Integer p_id) {
        return this.roomPersistence.getRoomById(p_id);
    }//Ok

    /**
     *
     * @param p_room
     * @return boolean
     */
    public boolean isRoomExistService(RoomEnum p_room) {
        return this.roomPersistence.isRoomExist(p_room);
    }//Ok

    /**
     * Get room by code
     *
     * @param p_code
     * @return RoomEnum
     */
    public RoomEnum getRoomByCodeService(String p_code) {
        return this.roomPersistence.getRoomByCode(p_code);
    }//Ok

    /**
     * Get room list
     *
     * @return List<RoomEnum>
     */
    public List<RoomEnum> getRoomsListService() {
        return this.roomPersistence.getRoomslist();
    }//Ok

    /**
     * Get HomeRoomProduct by id
     *
     * @param p_id
     * @return HomeRoomProduct
     */
    public HomeRoomProduct getHomeRoomProductByIdService(Integer p_id) {
        return this.homeRoomProductPersistence.getHomeRoomProductById(p_id);
    }//Ok

    /**
     * Get HomeRoomProducts list by HomeRoom
     *
     * @param p_homeRoom
     * @return List<HomeRoomProduct>
     */
    public List<HomeRoomProduct> getHomeRoomProductsByRoomHomeService(HomeRoom p_homeRoom) {
        return this.homeRoomProductPersistence.getHomeRoomProductsByRoomHome(this.homeRoomPersistence.getHRByHomeNRoom(p_homeRoom.getHome(),
                p_homeRoom.getRoom()));
    }//Ok

    /**
     * Add a product to HomeRoom
     *
     * @param p_homeRoom
     * @param p_product
     * @param p_alertQuantity
     * @param p_currentQuantity
     * @param p_limitQuantity
     * @param p_date
     */
    public void addProductToHomeRoomService(HomeRoom p_homeRoom, Product p_product, Integer p_alertQuantity, Integer p_currentQuantity, Integer p_limitQuantity, Date p_date) {

        HomeRoomProduct hrp = new HomeRoomProduct();

        hrp.setAlertQuantity(p_alertQuantity);
        hrp.setCurrentQuantity(p_currentQuantity);
        hrp.setHomeRoom(p_homeRoom);
        hrp.setLimitQuantity(p_limitQuantity);
        hrp.setPeremptionDate(p_date);
        hrp.setProduct(p_product);

        this.homeRoomProductPersistence.addProductToHomeRoom(hrp);
    }//Ok

    /**
     * Get HomeRoomProduct by Home and product
     *
     * @param p_homeRoom
     * @param p_product
     * @return HomeRoomProduct
     */
    public HomeRoomProduct getHomeRoomProductByHNPService(HomeRoom p_homeRoom, Product p_product) {
        HomeRoomProduct hrp = this.homeRoomProductPersistence.getHRPByHomeRoomNProduct(p_homeRoom, p_product);
        return hrp;
    }//Ok

    /**
     * Remove a product from HomeRoom
     *
     * @param p_homeRoomProduct
     */
    public void removeProductFromHomeRoomService(HomeRoomProduct p_homeRoomProduct) {
        this.homeRoomProductPersistence.removeProductFromHomeRoom(p_homeRoomProduct);
    }//Ok

    /**
     * Update HomeRoomProduct
     *
     * @param p_homeRoomProduct
     * @return HomeRoomProduct
     */
    public HomeRoomProduct updateHomeRoomProductService(HomeRoomProduct p_homeRoomProduct) {
        this.homeRoomProductPersistence.updateHomeRoomProduct(p_homeRoomProduct);
        return this.homeRoomProductPersistence.getHRPByHomeRoomNProduct(p_homeRoomProduct.getHomeRoom(),
                p_homeRoomProduct.getProduct());
    }//Ok 

    /**
     *
     * @param p_id
     * @return Product
     */
    public Product getProductByIdService(Integer p_id) {
        return this.productPersistence.getProductById(p_id);
    }//Ok

    /**
     *
     * @param p_code
     * @return Product
     */
    public Product getProductByCodeService(String p_code) {
        return this.productPersistence.getProductByCode(p_code);
    }//Ok

    /**
     *
     * @return Product
     */
    public Product getProductByLibelleService(String p_libelle) {
        return this.productPersistence.getProductByLibelle(p_libelle);
    }//Ok

    /**
     *
     * @return List<Product>
     */
    public List<Product> getProductsListService() {
        return this.productPersistence.getProductList();
    }//Ok

    /**
     *
     * @param p_product
     * @return Product
     */
    public Product createNewProductService(Product p_product) {
        this.productPersistence.createNewProduct(p_product);
        return this.productPersistence.getProductByCode(p_product.getCode());
    }//Ok

    /**
     *
     * @param p_product
     * @return Product
     */
    public Product updateProductService(Product p_product) {
        this.productPersistence.updateProduct(p_product);
        return this.productPersistence.getProductByCode(p_product.getCode());
    }//Ok

    /**
     *
     * @param p_product
     */
    public void deleteProductService(Product p_product) {
        this.productPersistence.deleteProduct(this.productPersistence.getProductByCode(p_product.getCode()));
    }//Ok

    /**
     *
     * @param p_id
     * @return UnityQuantityEnum
     */
    public UnityQuantityEnum getUnityQuantityByIdService(Integer p_id) {
        return this.unityQuantityPersistence.getUnityQuantityId(p_id);
    }//Ok

    /**
     *
     * @param p_code
     * @return UnityQuantityEnum
     */
    public UnityQuantityEnum getUnityQuantityByCodeService(String p_code) {
        return this.unityQuantityPersistence.getUnityQuantityCode(p_code);
    }//Ok

    /**
     *
     * @return List<UnityQuantityEnum>
     */
    public List<UnityQuantityEnum> getUnityQuantityListService() {
        return this.unityQuantityPersistence.getUnityQuantityList();
    }//Ok

}
