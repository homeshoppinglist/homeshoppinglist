package com.homeshoppinglistApp.business;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author BorisK <klettboris@gmail.com>
 */
@Entity
@Table(name = "UTILISATEURS", schema = "HOMESHOPPINGLIST")
public class User implements Serializable {

    private static final long serialVersionUID = 1L;

     @Id
    @Column(name = "numero", nullable = false, length = 30)
    @GeneratedValue(generator = "SEQ_UTIL", strategy = GenerationType.SEQUENCE)
    private Integer id;

    @Column(name = "prenom", nullable = false, length = 150)
    private String firstname;

    @Column(name = "nom", nullable = false, length = 150)
    private String lastname;

    @Column(name = "email", nullable = false, length = 150)
    private String email;

    @Column(name = "motdepasse", nullable = false, length = 150)
    private String password;

    @Temporal(TemporalType.DATE)
    @Column(name = "datenaissance", nullable = false, length = 150)
    private Date birthdate;

    @Column(name = "valid", nullable = false, length = 150)
    private Integer valid;

    @OneToMany(cascade = {CascadeType.ALL, CascadeType.REMOVE, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH, CascadeType.DETACH}, orphanRemoval = true, fetch = FetchType.LAZY)
    @JoinColumn(name = "UTIL_NUMERO")
    private List<Membre> members;

    /**
     * This is the constructor without params
     */
    public User() {
    }

    /**
     * This is the constructor with params
     *
     * @param id
     * @param firstname
     * @param lastname
     * @param email
     * @param password
     * @param birthdate
     * @param valid
     * @param members
     */
    public User(Integer id, String firstname, String lastname, String email, String password, Date birthdate, Integer valid, List<Membre> members) {
        this.id = id;
        this.firstname = firstname;
        this.lastname = lastname;
        this.email = email;
        this.password = password;
        this.birthdate = birthdate;
        this.valid = valid;
        this.members.addAll(members);
    }

    /**
     * This is the constructor without id
     *
     * @param firstname
     * @param lastname
     * @param email
     * @param password
     * @param birthdate
     * @param valid
     * @param members
     */
    public User(String firstname, String lastname, String email, String password, Date birthdate, Integer valid, List<Membre> members) {
        this.firstname = firstname;
        this.lastname = lastname;
        this.email = email;
        this.password = password;
        this.birthdate = birthdate;
        this.valid = valid;
        this.members.addAll(members);
    }

    /**
     * This is the constructor without id and valid params
     *
     * @param firstname
     * @param lastname
     * @param email
     * @param password
     * @param birthdate
     * @param members
     */
    public User(String firstname, String lastname, String email, String password, Date birthdate, List<Membre> members) {
        this.firstname = firstname;
        this.lastname = lastname;
        this.email = email;
        this.password = password;
        this.birthdate = birthdate;
        this.members = members;
    }

    /**
     * This is the id getter
     *
     * @return id
     */
    public Integer getId() {
        return id;
    }

    /**
     * This is the id setter
     *
     * @param id
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * this is the firstname getter
     *
     * @return firstname
     */
    public String getFirstname() {
        return firstname;
    }

    /**
     * this is the firstname setter
     *
     * @param firstname
     */
    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    /**
     * this is the lastname getter
     *
     * @return lastname
     */
    public String getLastname() {
        return lastname;
    }

    /**
     * this is the lastname setter
     *
     * @param lastname
     */
    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    /**
     * this is the email getter
     *
     * @return email
     */
    public String getEmail() {
        return email;
    }

    /**
     * this is the email setter
     *
     * @param email
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     * this is the password getter
     *
     * @return password
     */
    public String getPassword() {
        return password;
    }

    /**
     * this is the password setter
     *
     * @param password
     */
    public void setPassword(String password) {
        this.password = password;
    }

    /**
     * this is the birthdate getter
     *
     * @return birthdate
     */
    public Date getBirthdate() {
        return birthdate;
    }

    /**
     * this is the birtdate setter
     *
     * @param birthdate
     */
    public void setBirthdate(Date birthdate) {
        this.birthdate = birthdate;
    }

    /**
     * this is the valid getter
     *
     * @return valid
     */
    public Integer getValid() {
        return valid;
    }

    /**
     * this is the valid setter
     *
     * @param valid
     */
    public void setValid(Integer valid) {
        this.valid = valid;
    }

    /**
     * This is the Members list getter
     *
     * @return Members list
     */
    public List<Membre> getMembers() {
        return members;
    }

    /**
     * This method allow to add a membership (home) to a user
     *
     * @param p_member
     */
    public void addMember(Membre p_member) {
        this.getMembers().add(p_member);
        p_member.setUser(this);
    }

}
