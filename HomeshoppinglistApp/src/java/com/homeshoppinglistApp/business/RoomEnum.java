package com.homeshoppinglistApp.business;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 *
 * @author BorisK <klettboris@gmail.com>
 */
@Entity
@Table(name = "PIECES", schema = "HOMESHOPPINGLIST")
public class RoomEnum implements Serializable {

    private static final long serialVersionUID = 1L;

     @Id
    @Column(name = "numero", nullable = false, length = 30)
    @GeneratedValue(generator = "SEQ_PCS", strategy = GenerationType.SEQUENCE)
    private Integer id;

    @Column(name = "CODE", nullable = false, length = 150)
    private String code;

    @Column(name = "LIBELLE", nullable = false, length = 150)
    private String description;

    /**
     * This is the constructor
     */
    public RoomEnum() {
    }

    /**
     * This is the constructor with params
     *
     * @param code
     * @param description
     */
    public RoomEnum(String code, String description) {
        this.code = code;
        this.description = description;
    }

    /**
     * This is the constructor with params and the id
     *
     * @param id
     * @param code
     * @param description
     */
    public RoomEnum(Integer id, String code, String description) {
        this.id = id;
        this.code = code;
        this.description = description;
    }

    /**
     * This is the id getter
     *
     * @return the id
     */
    public Integer getId() {
        return id;
    }

    /**
     * This is the id setter
     *
     * @param id
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * This is the code getter
     *
     * @return the code
     */
    public String getCode() {
        return code;
    }

    /**
     * This is the code setter
     *
     * @param code
     */
    public void setCode(String code) {
        this.code = code;
    }

    /**
     * This is the description getter
     *
     * @return the description
     */
    public String getDescription() {
        return description;
    }

    /**
     * This is the description setter
     *
     * @param description
     */
    public void setDescription(String description) {
        this.description = description;
    }

}
