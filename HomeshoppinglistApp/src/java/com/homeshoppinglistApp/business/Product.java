package com.homeshoppinglistApp.business;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 *
 * @author BorisK <klettboris@gmail.com>
 */
@Entity
@Table(name = "PRODUITS", schema = "HOMESHOPPINGLIST")
public class Product implements Serializable {

    private static final long serialVersionUID = 1L;

     @Id
    @Column(name = "numero", nullable = false, length = 30)
    @GeneratedValue(generator = "SEQ_PRO", strategy = GenerationType.SEQUENCE)
    private Integer id;

    @Column(name = "code", nullable = false, length = 150)
    private String code;

    @Column(name = "nom", nullable = false, length = 150)
    private String libelle;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "UDQ_NUMERO", nullable = false)
    private UnityQuantityEnum unity;

    /**
     * This is the constructor without params
     */
    public Product() {
    }

    /**
     * This is the constructor without id
     *
     * @param code
     * @param libelle
     * @param unity
     */
    public Product(String code, String libelle, UnityQuantityEnum unity) {
        this.code = code;
        this.libelle = libelle;
        this.unity = unity;
    }

    /**
     * This is the constructor
     *
     * @param id
     * @param code
     * @param libelle
     * @param unity
     */
    public Product(Integer id, String code, String libelle, UnityQuantityEnum unity) {
        this.id = id;
        this.code = code;
        this.libelle = libelle;
        this.unity = unity;
    }

    /**
     * This is the product id getter
     *
     * @return product id
     */
    public Integer getId() {
        return id;
    }

    /**
     * This is the product id setter
     *
     * @param id
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * This is the product code getter
     *
     * @return product code
     */
    public String getCode() {
        return code;
    }

    /**
     * This is the product code setter
     *
     * @param code
     */
    public void setCode(String code) {
        this.code = code;
    }

    /**
     * This is the product libelle getter
     *
     * @return product libelle
     */
    public String getLibelle() {
        return libelle;
    }

    /**
     * This is the product libelle setter
     *
     * @param libelle
     */
    public void setLibelle(String libelle) {
        this.libelle = libelle;
    }

    /**
     * This is the product quantity unity getter
     *
     * @return product quantity unity
     */
    public UnityQuantityEnum getUnity() {
        return unity;
    }

    /**
     * This is the product quantity unity setter
     *
     * @param unity
     */
    public void setUnity(UnityQuantityEnum unity) {
        this.unity = unity;
    }

}
