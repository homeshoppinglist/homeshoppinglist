package com.homeshoppinglistApp.business;

import java.io.Serializable;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 *
 * @author BorisK <klettboris@gmail.com>
 */
@Entity
@Table(name = "PIECESDUFOYER", schema = "HOMESHOPPINGLIST")
public class HomeRoom implements Serializable {

    private static final long serialVersionUID = 1L;

     @Id
    @Column(name = "numero", nullable = false, length = 30)
    @GeneratedValue(generator = "SEQ_PDF", strategy = GenerationType.SEQUENCE)
    private Integer id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "FOY_NUMERO")
    private Home home;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "PCS_NUMERO")
    private RoomEnum room;

    @OneToMany(cascade = {CascadeType.ALL, CascadeType.REMOVE, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH, CascadeType.DETACH}, orphanRemoval = true, fetch = FetchType.LAZY)
    @JoinColumn(name = "PDF_NUMERO")
    private List<HomeRoomProduct> homeRoomProducts;

    /**
     * This is the constructor without params
     */
    public HomeRoom() {
    }

    /**
     * This is the constructor without id
     *
     * @param home
     * @param room
     */
    public HomeRoom(Home home, RoomEnum room) {
        this.home = home;
        this.room = room;
    }

    /**
     * this is the constructor
     *
     * @param id
     * @param home
     * @param room
     */
    public HomeRoom(Integer id, Home home, RoomEnum room) {
        this.id = id;
        this.home = home;
        this.room = room;
    }

    /**
     * This is the home room id getter
     *
     * @return home room id
     */
    public Integer getId() {
        return id;
    }

    /**
     * This is the home room id setter
     *
     * @param id
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * This is the home getter
     *
     * @return home
     */
    public Home getHome() {
        return home;
    }

    /**
     * This is the home setter
     *
     * @param home
     */
    public void setHome(Home home) {
        this.home = home;
    }

    /**
     * This is room getter
     *
     * @return room
     */
    public RoomEnum getRoom() {
        return room;
    }

    /**
     * This is room setter
     *
     * @param room
     */
    public void setRoom(RoomEnum room) {
        this.room = room;
    }

    /**
     * This is the homeRoomProducts getter
     *
     * @return
     */
    public List<HomeRoomProduct> getHomeRoomProducts() {
        return homeRoomProducts;
    }

    /**
     * This method allow to add a product to a home room
     *
     * @param p_homeRoomProduct
     */
    public void addHomeRoomProduct(HomeRoomProduct p_homeRoomProduct) {
        this.getHomeRoomProducts().add(p_homeRoomProduct);
        p_homeRoomProduct.setHomeRoom(this);
    }
}
