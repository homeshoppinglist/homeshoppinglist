package com.homeshoppinglistApp.business;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author BorisK <klettboris@gmail.com>
 */
@Entity
@Table(name = "PRODUITSPIECESFOYER", schema = "HOMESHOPPINGLIST")
public class HomeRoomProduct implements Serializable {

    private static final long serialVersionUID = 1L;

     @Id
    @Column(name = "numero", nullable = false, length = 30)
    @GeneratedValue(generator = "SEQ_PPF", strategy = GenerationType.SEQUENCE)
    private Integer id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "PDF_NUMERO")
    private HomeRoom homeRoom;

    @Column(name = "QUANTITELIMITE", nullable = false)
    private Integer limitQuantity;

    @Column(name = "QUANTITEACTUELLE", nullable = false)
    private Integer currentQuantity;

    @Column(name = "QUANTITEALERTE", nullable = false)
    private Integer alertQuantity;

    @Temporal(TemporalType.DATE)
    @Column(name = "DATEPEREMTION", nullable = false)
    private Date peremptionDate;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "PRO_NUMERO")
    private Product product;

    /**
     * This is the constructor without params
     */
    public HomeRoomProduct() {
    }

    /**
     * This is the constructor without id
     *
     * @param homeRoom
     * @param limitQuantity
     * @param currentQuantity
     * @param alertQuantity
     * @param peremptionDate
     * @param product
     */
    public HomeRoomProduct(HomeRoom homeRoom, Integer limitQuantity, Integer currentQuantity, Integer alertQuantity, Date peremptionDate, Product product) {
        this.homeRoom = homeRoom;
        this.limitQuantity = limitQuantity;
        this.currentQuantity = currentQuantity;
        this.alertQuantity = alertQuantity;
        this.peremptionDate = peremptionDate;
        this.product = product;
    }

    /**
     * This is the constructor
     *
     * @param id
     * @param homeRoom
     * @param limitQuantity
     * @param currentQuantity
     * @param alertQuantity
     * @param peremptionDate
     * @param product
     */
    public HomeRoomProduct(Integer id, HomeRoom homeRoom, Integer limitQuantity, Integer currentQuantity, Integer alertQuantity, Date peremptionDate, Product product) {
        this.id = id;
        this.homeRoom = homeRoom;
        this.limitQuantity = limitQuantity;
        this.currentQuantity = currentQuantity;
        this.alertQuantity = alertQuantity;
        this.peremptionDate = peremptionDate;
        this.product = product;
    }

    /**
     * This is the id getter
     *
     * @return id
     */
    public Integer getId() {
        return id;
    }

    /**
     * This is the id setter
     *
     * @param id
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * This is the room home getter
     *
     * @return home room
     */
    public HomeRoom getHomeRoom() {
        return homeRoom;
    }

    /**
     * This is the home room setter
     *
     * @param homeRoom
     */
    public void setHomeRoom(HomeRoom homeRoom) {
        this.homeRoom = homeRoom;
    }

    /**
     * This is the quantity limit getter
     *
     * @return quantity limit
     */
    public Integer getLimitQuantity() {
        return limitQuantity;
    }

    /**
     * This is the quantity limit setter
     *
     * @param limitQuantity
     */
    public void setLimitQuantity(Integer limitQuantity) {
        this.limitQuantity = limitQuantity;
    }

    /**
     * This is the current quantity getter
     *
     * @return current quantity
     */
    public Integer getCurrentQuantity() {
        return currentQuantity;
    }

    /**
     * This is the current quantity setter
     *
     * @param currentQuantity
     */
    public void setCurrentQuantity(Integer currentQuantity) {
        this.currentQuantity = currentQuantity;
    }

    /**
     * This is the alert quantity getter
     *
     * @return alert quantity
     */
    public Integer getAlertQuantity() {
        return alertQuantity;
    }

    /**
     * This is the alert quantity setter
     *
     * @param alertQuantity
     */
    public void setAlertQuantity(Integer alertQuantity) {
        this.alertQuantity = alertQuantity;
    }

    /**
     * This is the peremption date getter
     *
     * @return peremption date
     */
    public Date getPeremptionDate() {
        return peremptionDate;
    }

    /**
     * This is the peremption date setter
     *
     * @param peremptionDate
     */
    public void setPeremptionDate(Date peremptionDate) {
        this.peremptionDate = peremptionDate;
    }

    /**
     * This is the product getter
     *
     * @return product
     */
    public Product getProduct() {
        return product;
    }

    /**
     * This is the product setter
     *
     * @param product
     */
    public void setProduct(Product product) {
        this.product = product;
    }

}
