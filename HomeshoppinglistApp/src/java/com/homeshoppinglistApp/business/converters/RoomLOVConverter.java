package com.homeshoppinglistApp.business.converters;

import com.homeshoppinglistApp.business.RoomEnum;
import com.homeshoppinglistApp.services.Service;
import java.util.ArrayList;
import java.util.List;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.ConverterException;
import javax.faces.convert.FacesConverter;
import javax.inject.Inject;

/**
 *
 * @author BorisK
 */
@FacesConverter(forClass = RoomLOVConverter.class, value = "roomLOVConverter")
public class RoomLOVConverter implements Converter {

    @Inject
    Service service;

    //Permet d'obtenir un objet statut en fonction de la saisie de l'utilisateur, par le paramètre value 
    /**
     *
     * @param context
     * @param component
     * @param value
     * @return
     */
    @Override
    public Object getAsObject(FacesContext context, UIComponent component, String value) {
        List<RoomEnum> roomList = new ArrayList<>();
        RoomEnum r1 = new RoomEnum();
        r1.setId(1);
        r1.setCode("rt1");
        r1.setDescription("RoomTest1");

        RoomEnum r2 = new RoomEnum();
        r2.setId(2);
        r2.setCode("rt2");
        r2.setDescription("RoomTest2");

        RoomEnum r3 = new RoomEnum();
        r3.setId(3);
        r3.setCode("rt3");
        r3.setDescription("RoomTest3");

        RoomEnum r4 = new RoomEnum();
        r4.setId(4);
        r4.setCode("rt4");
        r4.setDescription("RoomTest4");

        roomList.add(r1);
        roomList.add(r2);
        roomList.add(r3);
        roomList.add(r4);

        if (value == null) {
            return null;
        }

        for (RoomEnum r : roomList) {
            if (r.getId().toString().equals(value)) {
                return r;
            }
        }
        throw new ConverterException(new FacesMessage(String.format("Cannot convert %s to Client", value)));
    }

    /**
     * 
     * @param context
     * @param component
     * @param value
     * @return 
     */
    @Override
    public String getAsString(FacesContext context, UIComponent component, Object value) {
        return (value instanceof RoomEnum) ? String.valueOf(((RoomEnum) value).getId()) : null;
    }

}
