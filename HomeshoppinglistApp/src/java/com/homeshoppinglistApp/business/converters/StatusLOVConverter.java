package com.homeshoppinglistApp.business.converters;

import com.homeshoppinglistApp.business.UserStatusEnum;
import com.homeshoppinglistApp.services.Service;
import java.util.ArrayList;
import java.util.List;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;
import javax.inject.Inject;

/**
 *
 * @author BorisK
 */
@FacesConverter(forClass = StatusLOVConverter.class, value = "statusLOVConverter")
public class StatusLOVConverter implements Converter {

    @Inject
    Service service;

    //Permet d'obtenir un objet statut en fonction de la saisie de l'utilisateur, par le paramètre value 
    /**
     *
     * @param context
     * @param component
     * @param value
     * @return
     */
    @Override
    public Object getAsObject(FacesContext context, UIComponent component, String value) {
        List<UserStatusEnum> statusList = new ArrayList<>();
        UserStatusEnum st = new UserStatusEnum();
        st.setId(1);
        st.setCode("Memb");
        st.setDescription("Member");
        statusList.add(st);
        st = new UserStatusEnum();
        st.setId(2);
        st.setCode("Admin");
        st.setDescription("Administrator");
        statusList.add(st);

        if (value == null) {
            return null;
        } else {
            //UserStatusEnum status = service.getUserStatusByIdService(Integer.parseInt(value));
            UserStatusEnum status = new UserStatusEnum();
            for (UserStatusEnum us : statusList) {
                if (us.getCode().equalsIgnoreCase(value)) {
                    status = us;
                }
            }
            return status;
        }
    }

    //Défini la valeur String qui sera utilisée comme valeur de l'attribut html value
    /**
     *
     * @param context
     * @param component
     * @param value
     * @return
     */
    @Override
    public String getAsString(FacesContext context, UIComponent component, Object value) {
        if (value == null) {
            return "";
        } else if (value instanceof UserStatusEnum) {
            return (String.valueOf(((UserStatusEnum) value).getId()));
        } else {
            return "";
        }
    }

}
