package com.homeshoppinglistApp.business;

import java.io.Serializable;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 *
 * @author BorisK <klettboris@gmail.com>
 */
@Entity
@Table(name = "FOYERS", schema = "HOMESHOPPINGLIST")
public class Home implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "numero", nullable = false, length = 30)
    @GeneratedValue(generator = "SEQ_FOY", strategy = GenerationType.SEQUENCE)
    private Integer id;

    @Column(name = "NOM", nullable = false, length = 150)
    private String name;

    @Column(name = "RUE", nullable = false, length = 150)
    private String street;

    @Column(name = "CODEPOSTAL", nullable = false, length = 150)
    private String zipCode;

    @Column(name = "VILLE", nullable = false, length = 150)
    private String city;

    @OneToMany(cascade = {CascadeType.ALL, CascadeType.REMOVE, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH, CascadeType.DETACH}, orphanRemoval = true, fetch = FetchType.LAZY)
    @JoinColumn(name = "FOY_NUMERO")
    private List<Membre> members;

    @OneToMany(cascade = {CascadeType.ALL, CascadeType.REMOVE, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH, CascadeType.DETACH}, orphanRemoval = true, fetch = FetchType.LAZY)
    @JoinColumn(name = "FOY_NUMERO")
    private List<HomeRoom> homeRooms;

    /**
     * This is the constructor without params
     */
    public Home() {
    }

    /**
     * This is the constructor without id
     *
     * @param name
     * @param street
     * @param zipCode
     * @param city
     * @param members
     * @param homeRooms
     */
    public Home(String name, String street, String zipCode, String city, List<Membre> members, List<HomeRoom> homeRooms) {
        this.name = name;
        this.street = street;
        this.zipCode = zipCode;
        this.city = city;
        this.members = members;
        this.homeRooms = homeRooms;
    }

    /**
     * This is the constructor
     *
     * @param id
     * @param name
     * @param street
     * @param zipCode
     * @param city
     * @param members
     * @param homeRooms
     */
    public Home(Integer id, String name, String street, String zipCode, String city, List<Membre> members, List<HomeRoom> homeRooms) {
        this.id = id;
        this.name = name;
        this.street = street;
        this.zipCode = zipCode;
        this.city = city;
        this.members = members;
        this.homeRooms = homeRooms;
    }

    /**
     * This is the id getter
     *
     * @return home id
     */
    public Integer getId() {
        return id;
    }

    /**
     * This is the id setter
     *
     * @param id
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * This is the home name getter
     *
     * @return home name
     */
    public String getName() {
        return name;
    }

    /**
     * This is the home name setter
     *
     * @param name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * This is the home street getter
     *
     * @return home street
     */
    public String getStreet() {
        return street;
    }

    /**
     * This is the home street setter
     *
     * @param street
     */
    public void setStreet(String street) {
        this.street = street;
    }

    /**
     * This is the home zip code getter
     *
     * @return home zip code
     */
    public String getZipCode() {
        return zipCode;
    }

    /**
     * This is the home zip code setter
     *
     * @param zipCode
     */
    public void setZipCode(String zipCode) {
        this.zipCode = zipCode;
    }

    /**
     * This is the home city getter
     *
     * @return home city
     */
    public String getCity() {
        return city;
    }

    /**
     * This is the home city setter
     *
     * @param city
     */
    public void setCity(String city) {
        this.city = city;
    }

    /**
     * This is the home members getter
     *
     * @return list of members
     */
    public List<Membre> getMembers() {
        return members;
    }

    /**
     * This method allow to add a membership (user) to a home
     *
     * @param p_member
     */
    public void addMember(Membre p_member) {
        this.getMembers().add(p_member);
        p_member.setHome(this);
    }

    /**
     * This is the home rooms getter
     *
     * @return home rooms
     */
    public List<HomeRoom> getHomeRooms() {
        return homeRooms;
    }

    /**
     * This method allow to add a room to a home
     *
     * @param p_homeRoom
     */
    public void addHomeRoom(HomeRoom p_homeRoom) {
        this.getHomeRooms().add(p_homeRoom);
        p_homeRoom.setHome(this);
    }

}
