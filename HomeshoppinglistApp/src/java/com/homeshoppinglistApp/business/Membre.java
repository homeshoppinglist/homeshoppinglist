package com.homeshoppinglistApp.business;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author BorisK <klettboris@gmail.com>
 */
@Entity
@Table(name = "MEMBRES", schema = "HOMESHOPPINGLIST")
public class Membre implements Serializable {

    private static final long serialVersionUID = 1L;

     @Id
    @Column(name = "numero", nullable = false, length = 30)
    @GeneratedValue(generator = "SEQ_MEM", strategy = GenerationType.SEQUENCE)
    private Integer id;

    @Temporal(TemporalType.DATE)
    @Column(name = "DATEDEBUT", nullable = false)
    private Date startDate;

    @Temporal(TemporalType.DATE)
    @Column(name = "DATEFIN", nullable = false)
    private Date endDate;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "ROL_NUMERO", nullable = false)
    private UserStatusEnum status;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "UTIL_NUMERO")
    private User user;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "FOY_NUMERO")
    private Home home;

    /**
     * This is the constructor without params
     */
    public Membre() {
    }

    /**
     * This is the constructor without id
     *
     * @param startDate
     * @param endDate
     * @param status
     * @param user
     * @param home
     */
    public Membre(Date startDate, Date endDate, UserStatusEnum status, User user, Home home) {
        this.startDate = startDate;
        this.endDate = endDate;
        this.status = status;
        this.user = user;
        this.home = home;
    }

    /**
     * This is the constructor
     *
     * @param id
     * @param startDate
     * @param endDate
     * @param status
     * @param user
     * @param home
     */
    public Membre(Integer id, Date startDate, Date endDate, UserStatusEnum status, User user, Home home) {
        this.id = id;
        this.startDate = startDate;
        this.endDate = endDate;
        this.status = status;
        this.user = user;
        this.home = home;
    }

    /**
     * This is the id getter
     *
     * @return id
     */
    public Integer getId() {
        return id;
    }

    /**
     * This is the id setter
     *
     * @param id
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * This is the start date getter
     *
     * @return startDate
     */
    public Date getStartDate() {
        return startDate;
    }

    /**
     * This is the start date setter
     *
     * @param startDate
     */
    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    /**
     * This is the end date getter
     *
     * @return endDate
     */
    public Date getEndDate() {
        return endDate;
    }

    /**
     * This is the end date setter
     *
     * @param endDate
     */
    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    /**
     * This is the status getter
     *
     * @return UserStatusEnum
     */
    public UserStatusEnum getStatus() {
        return status;
    }

    /**
     * This is the user status setter
     *
     * @param status
     */
    public void setStatus(UserStatusEnum status) {
        this.status = status;
    }

    /**
     * This is the user getter
     *
     * @return User
     */
    public User getUser() {
        return user;
    }

    /**
     * This is the user setter
     *
     * @param user
     */
    public void setUser(User user) {
        this.user = user;
    }

    /**
     * This is the home getter
     *
     * @return home
     */
    public Home getHome() {
        return home;
    }

    /**
     * This is the home setter
     *
     * @param home
     */
    public void setHome(Home home) {
        this.home = home;
    }

}
