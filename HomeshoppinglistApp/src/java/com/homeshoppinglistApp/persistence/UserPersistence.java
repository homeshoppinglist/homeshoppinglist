package com.homeshoppinglistApp.persistence;

import com.homeshoppinglistApp.business.User;
import com.homeshoppinglistApp.services.Service;
import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.Query;

/**
 * This class allow to manage user data from the data base
 *
 * @author BorisK <klettboris@gmail.com>
 */
@Stateless
public class UserPersistence implements Serializable {

    @Inject
    private Connexion con;
    private static final long serialVersionUID = 1L;

    /**
     * This is the constructor
     */
    public UserPersistence() {
    }

    /**
     * This method return a user from its id
     *
     * @param p_id researched user id
     * @return user
     */
    public User getUserById(Integer p_id) {
        User user;
        user = (User) con.getEM().find(User.class, p_id);
        return user;
    }//Ok

    /**
     * This method return a user from its email
     *
     * @param p_email researched user email
     * @return user
     */
    public User getUserByEmail(String p_email) {

        User user;
        try {
            Query query = con.getEM().createNativeQuery("select * from utilisateurs where email = ?", User.class)
                    .setParameter(1, p_email);

            user = (User) query.getSingleResult();
        } catch (Exception ex) {
            user = null;
        }
        return user;
    }//Ok

    /**
     * This method return a user from its infos
     *
     * @param p_firstname researched user's firstname
     * @param p_lastname researched user's lastname
     * @param p_birthdate researched user's birthdate
     * @return user
     */
    public User getUserByInfos(String p_firstname, String p_lastname, Date p_birthdate) {
        User user;
        try {
            Query query = con.getEM().createNativeQuery("select * from utilisateurs where UPPER (nom) = UPPER (?) and  UPPER (prenom) =UPPER (?) and DATENAISSANCE = to_date(?, 'dd.mm.yy')", User.class)
                    .setParameter(1, p_lastname)
                    .setParameter(2, p_firstname)
                    .setParameter(3, Service.FORMATTER_FULL_YEAR_FORMAT.format(p_birthdate));

            user = (User) query.getSingleResult();
        } catch (Exception ex) {
            user = null;
        }

        return user;
    }//Ok

    /**
     * This method create a new user
     *
     * @param p_user
     * @return the new user
     */
    public User createNewUser(User p_user) {
        String minutes = Service.H.format(new Date());
        User user;

        p_user.setValid(Integer.valueOf(minutes));
        con.getEM().persist(p_user);

        user = this.getUserByEmail(p_user.getEmail());
        return user;
    }//Ok

    /**
     * This method update a user
     *
     * @param p_user updated user
     */
    public void updateUser(User p_user) {
        User user = this.getUserById(p_user.getId());

        user.setBirthdate(p_user.getBirthdate());
        user.setEmail(p_user.getEmail());
        user.setFirstname(p_user.getFirstname());
        user.setLastname(p_user.getLastname());
        user.setPassword(p_user.getPassword());

        con.getEM().flush();
    }//Ok

    /**
     * This method delete a user
     *
     * @param p_user deleted user
     */
    public void deleteUser(User p_user) {
        User user = this.getUserById(p_user.getId());
        con.getEM().remove(user);
    }//Ok

    /**
     * This method return a list of user
     *
     * @return list of user
     */
    public List<User> getUserList() {
        List<User> users;
        users = new ArrayList<>();

        Query query = con.getEM().createNativeQuery("select * from utilisateurs", User.class);
        users.addAll((List<User>) query.getResultList());

        return users;
    }//Ok

    /**
     * This method allow a user to connect
     *
     * @param p_userEmail
     * @param p_userPwd
     * @return the user
     */
    public User userConnect(String p_userEmail, String p_userPwd) {

        User user;

        try {
            Query query = con.getEM().createNativeQuery("select * from utilisateurs where email = ? and motdepasse=?", User.class)
                    .setParameter(1, p_userEmail)
                    .setParameter(2, p_userPwd);

            user = (User) query.getSingleResult();

            if (user != null) {
                user.setValid(200);
                con.getEM().flush();
            }

        } catch (Exception ex) {
            return null;
        }
        return user;
    }//Ok

}
