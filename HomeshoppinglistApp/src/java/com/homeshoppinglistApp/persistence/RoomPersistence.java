package com.homeshoppinglistApp.persistence;

import com.homeshoppinglistApp.business.RoomEnum;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.Query;

/**
 * This class allow to manage room data
 *
 * @author BorisK <klettboris@gmail.com>
 */
@Stateless
public class RoomPersistence implements Serializable {

    @Inject
    private Connexion con;
    private static final long serialVersionUID = 1L;

    /**
     * This is the constructor
     */
    public RoomPersistence() {
    }

    /**
     * This method allow to get a room with its id
     *
     * @param p_id
     * @return room
     */
    public RoomEnum getRoomById(Integer p_id) {
        RoomEnum room;
        room = (RoomEnum) con.getEM().find(RoomEnum.class, p_id);

        return room;
    }//Ok

    /**
     * This method allow to know if a room exists
     *
     * @param p_room searched room
     * @return boolean
     */
    public boolean isRoomExist(RoomEnum p_room) {
        boolean isOk = false;
        RoomEnum room;
        Query query = con.getEM().createNativeQuery("select * from PIECES where UPPER(code) = UPPER(?) and UPPER(libelle) = UPPER(?)", RoomEnum.class)
                .setParameter(1, p_room.getCode())
                .setParameter(2, p_room.getDescription());
        try {
            room = (RoomEnum) query.getSingleResult();
            isOk = true;
            if (room == null) {
                isOk = false;
            }
        } catch (Exception ex) {
            return isOk;
        }

        return isOk;
    }//Ok

    /**
     * This method allow to get a room with its code
     *
     * @param p_code
     * @return the research room
     */
    public RoomEnum getRoomByCode(String p_code) {
        RoomEnum room;
        Query query = con.getEM().createNativeQuery("select * from PIECES where UPPER(code) = UPPER(?)", RoomEnum.class)
                .setParameter(1, p_code);

        room = (RoomEnum) query.getSingleResult();

        return room;
    }//Ok

    /**
     * This method return the rooms list
     *
     * @return a list of rooms
     */
    public List<RoomEnum> getRoomslist() {
        List<RoomEnum> rooms;
        Query query = con.getEM().createNativeQuery("select * from PIECES", RoomEnum.class);

        rooms = (List<RoomEnum>) query.getResultList();
        if (rooms == null) {
            return new ArrayList<>();
        }

        return rooms;
    }//Ok

}
