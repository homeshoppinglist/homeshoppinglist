package com.homeshoppinglistApp.persistence;

import com.homeshoppinglistApp.business.Product;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.Query;

/**
 * This class allow to manage product data from the data base
 *
 * @author BorisK <klettboris@gmail.com>
 */
@Stateless
public class ProductPersistence implements Serializable {

    @Inject
    private Connexion con;
    private static final long serialVersionUID = 1L;

    /**
     * This is the constructor
     */
    public ProductPersistence() {
    }

    /**
     * This method allow to get a product by its id
     *
     * @param p_id
     * @return product
     */
    public Product getProductById(Integer p_id) {
        Product product;
        product = (Product) con.getEM().find(Product.class, p_id);

        return product;
    }//Ok

    /**
     * This method allow to get a product by its code
     *
     * @param p_code
     * @return product
     */
    public Product getProductByCode(String p_code) {
        Product product;
        try {
            Query query = con.getEM().createNativeQuery("select * from PRODUITS where UPPER(code) = UPPER(?)", Product.class)
                    .setParameter(1, p_code);
            product = (Product) query.getSingleResult();
        } catch (Exception ex) {
            product = null;
        }

        return product;
    }//Ok

    /**
     * This method allow to get a product by its libelle
     *
     * @return product
     */
    public Product getProductByLibelle(String p_libelle) {
        Product product;
        try {
            Query query = con.getEM().createNativeQuery("select * from PRODUITS where UPPER(nom) = UPPER(?)", Product.class)
                    .setParameter(1, p_libelle);
            product = (Product) query.getSingleResult();
        } catch (Exception ex) {
            product = null;
        }

        return product;
    }//Ok

    /**
     * get all products
     *
     * @return list of product
     */
public List<Product> getProductList() {
        List<Product> products;
        Query query = con.getEM().createNativeQuery("select * from PRODUITS", Product.class);
        products = (List<Product>) query.getResultList();
        if (products == null) {
            return new ArrayList<>();
        }

        return products;
    }//Ok

    /**
     * Create a new product
     *
     * @param p_product
     */
    public void createNewProduct(Product p_product) {
        con.getEM().persist(p_product);
    }//Ok

    /**
     * This method allow to update a product
     *
     * @param p_product
     */
    public void updateProduct(Product p_product) {

        Product product = this.getProductById(p_product.getId());

        product.setId(p_product.getId());
        product.setCode(p_product.getCode());
        product.setLibelle(p_product.getLibelle());
        product.setUnity(p_product.getUnity());

        con.getEM().flush();

    }//Ok

    /**
     * This method allow to delete a product
     *
     * @param p_product
     */
    public void deleteProduct(Product p_product) {

        Product product = this.getProductByCode(p_product.getCode());
        con.getEM().remove(product);

    }//Ok

}
