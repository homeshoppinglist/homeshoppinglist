package com.homeshoppinglistApp.persistence;

import com.homeshoppinglistApp.business.HomeRoom;
import com.homeshoppinglistApp.business.HomeRoomProduct;
import com.homeshoppinglistApp.business.Product;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.Query;

/**
 *
 * This class allow to manage home room product data from the data base
 *
 * @author BorisK <klettboris@gmail.com>
 */
@Stateless
public class HomeRoomProductPersistence implements Serializable {

    @Inject
    private Connexion con;
    private static final long serialVersionUID = 1L;

    /**
     * This is the constructor
     */
    public HomeRoomProductPersistence() {
    }

    /**
     * This method allow the get the product home room by id
     *
     * @param p_id researched home room product id
     * @return home roomm product
     */
    public HomeRoomProduct getHomeRoomProductById(Integer p_id) {
        HomeRoomProduct homeRoomProduct;
        homeRoomProduct = (HomeRoomProduct) con.getEM().find(HomeRoomProduct.class, p_id);
        return homeRoomProduct;
    }//Ok

    /**
     * This method allow to get the list of the product home room by home room
     *
     * @param p_homeRoom
     * @return list of home room product
     */
    public List<HomeRoomProduct> getHomeRoomProductsByRoomHome(HomeRoom p_homeRoom) {
        List<HomeRoomProduct> homeRoomProducts;
        try {
            Query query = con.getEM().createNativeQuery("select * from PRODUITSPIECESFOYER where PDF_NUMERO = ?", HomeRoomProduct.class)
                    .setParameter(1, p_homeRoom.getId());
            homeRoomProducts = (List<HomeRoomProduct>) query.getResultList();
        } catch (Exception ex) {
            return new ArrayList<>();
        }

        return homeRoomProducts;
    }//Ok

    /**
     * add a product to home room
     *
     * @param p_homeRoomProduct the new home room product
     */
    public void addProductToHomeRoom(HomeRoomProduct p_homeRoomProduct) {
        HomeRoomProduct hrp = new HomeRoomProduct();
        hrp.setAlertQuantity(p_homeRoomProduct.getAlertQuantity());
        hrp.setCurrentQuantity(p_homeRoomProduct.getCurrentQuantity());
        hrp.setHomeRoom(p_homeRoomProduct.getHomeRoom());
        hrp.setLimitQuantity(p_homeRoomProduct.getLimitQuantity());
        hrp.setPeremptionDate(p_homeRoomProduct.getPeremptionDate());
        hrp.setProduct(p_homeRoomProduct.getProduct());

        con.getEM().persist(hrp);
    }//Ok

    /**
     * This method allow to remove a product from a home room
     *
     * @param p_homeRoomProduct
     */
    public void removeProductFromHomeRoom(HomeRoomProduct p_homeRoomProduct) {
        HomeRoomProduct homeRoomProduct = this.getHomeRoomProductById(p_homeRoomProduct.getId());

        con.getEM().remove(homeRoomProduct);

    }//Ok

    /**
     * This method allow to update a product in a home room
     *
     * @param p_homeRoomProduct
     */
    public void updateHomeRoomProduct(HomeRoomProduct p_homeRoomProduct) {
        HomeRoomProduct homeRoomProduct = this.getHomeRoomProductById(p_homeRoomProduct.getId());

        homeRoomProduct.setId(p_homeRoomProduct.getId());
        homeRoomProduct.setAlertQuantity(p_homeRoomProduct.getAlertQuantity());
        homeRoomProduct.setCurrentQuantity(p_homeRoomProduct.getCurrentQuantity());
        homeRoomProduct.setLimitQuantity(p_homeRoomProduct.getLimitQuantity());
        homeRoomProduct.setPeremptionDate(p_homeRoomProduct.getPeremptionDate());

        con.getEM().flush();
    }//Ok

    /**
     * This method allow to get a home room Product by a given homeroom and a
     * given product
     *
     * @param p_homeRoom
     * @param p_product
     * @return
     */
    public HomeRoomProduct getHRPByHomeRoomNProduct(HomeRoom p_homeRoom, Product p_product) {
        HomeRoomProduct hrp;
        try {
            Query query = con.getEM().createNativeQuery("select * from PRODUITSPIECESFOYER where PDF_NUMERO = ? AND PRO_NUMERO=?", HomeRoomProduct.class)
                    .setParameter(1, p_homeRoom.getId())
                    .setParameter(2, p_product.getId());

            hrp = (HomeRoomProduct) query.getSingleResult();
        } catch (Exception ex) {
            hrp = null;
        }
        return hrp;

    }//New

}
