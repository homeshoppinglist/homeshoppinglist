package com.homeshoppinglistApp.persistence;

import com.homeshoppinglistApp.business.Home;
import java.io.Serializable;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityTransaction;
import javax.persistence.Query;

/**
 *
 * This class allow to manage home data from the data base
 *
 * @author BorisK <klettboris@gmail.com>
 */
@Stateless
public class HomePersistence implements Serializable {

    @Inject
    private Connexion con;
    private static final long serialVersionUID = 1L;

    /**
     * This is the constructor
     */
    public HomePersistence() {
    }

    /**
     * This method allow to get Home by its name
     *
     * @param p_name
     * @return Home
     */
    public Home getHomeByName(String p_name) {

        Home home;
        try {
            Query query = con.getEM().createNativeQuery("select * from foyers where UPPER(nom) = UPPER(?) ", Home.class)
                    .setParameter(1, p_name);
            home = (Home) query.getSingleResult();
        } catch (Exception ex) {
            home = null;
        }
        return home;
    }//Ok

    /**
     * This method allow to get home by its address
     *
     * @param p_street
     * @param p_zipCode
     * @param p_city
     * @return Home
     */
    public Home getHomeByAddress(String p_street, String p_zipCode, String p_city) {
        Home home;

        try {
            Query query = con.getEM().createNativeQuery("select * from foyers where UPPER(REPLACE(TRIM(RUE),' ','')) = UPPER(REPLACE(TRIM(?),' ','')) AND CODEPOSTAL = ? AND UPPER(REPLACE(TRIM(VILLE),' ','')) = UPPER(REPLACE(TRIM(?),' ','')) ", Home.class)
                    .setParameter(1, p_street)
                    .setParameter(2, p_zipCode)
                    .setParameter(3, p_city);

            home = (Home) query.getSingleResult();

        } catch (Exception ex) {
            home = null;
        }

        return home;
    }//Ok

    /**
     * This method allow to create a new home
     *
     * @param p_home
     */
    public void createNewHome(Home p_home) {
        Home home = new Home();
        home.setCity(p_home.getCity());
        home.setName(p_home.getName());
        home.setStreet(p_home.getStreet());
        home.setZipCode(p_home.getZipCode());
        con.getEM().persist(p_home);
    }//Ok

    public void createNewHome2(Home p_home) {
        EntityTransaction transaction = con.getEM().getTransaction();
        transaction.begin();
        con.getEM().persist(p_home);
        transaction.commit();
    }

    /**
     * This method allow to delete a home
     *
     * @param p_home
     */
    public void deleteHome(Home p_home) {
        Home home = (Home) con.getEM().find(Home.class, p_home.getId());
        con.getEM().remove(home);
    }//Ok

    /**
     * This method allow to update a home
     *
     * @param p_home
     */
    public void updateHome(Home p_home) {

        Home home = (Home) con.getEM().find(Home.class, p_home.getId());

        home.setId(p_home.getId());
        home.setCity(p_home.getCity());
        home.setStreet(p_home.getStreet());
        home.setZipCode(p_home.getZipCode());
        home.setName(p_home.getName());

        con.getEM().flush();
    }//Ok
}
