package com.homeshoppinglistApp.persistence;

import com.homeshoppinglistApp.business.UnityQuantityEnum;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.Query;

/**
 * This class allow to manage unity quantity data from the data base
 *
 * @author BorisK <klettboris@gmail.com>
 */
@Stateless
public class UnityQuantityPersistence implements Serializable {

    @Inject
    private Connexion con;
    private static final long serialVersionUID = 1L;

    /**
     * This is the constructor
     */
    public UnityQuantityPersistence() {
    }

    /**
     * This method return a quantity's unity from its id
     *
     * @param p_id
     * @return UnityQuantityEnum
     */
    public UnityQuantityEnum getUnityQuantityId(Integer p_id) {
        UnityQuantityEnum unityQuantity;

        unityQuantity = (UnityQuantityEnum) con.getEM().find(UnityQuantityEnum.class, p_id);

        return unityQuantity;
    }//Ok

    /**
     * This method return a quantity's unity from its code
     *
     * @param p_code
     * @return UnityQuantityEnum
     */
    public UnityQuantityEnum getUnityQuantityCode(String p_code) {
        UnityQuantityEnum unityQuantity;

        Query query = con.getEM().createNativeQuery("select * from UNITESDEQUANTITE where upper(CODE) = upper(?)", UnityQuantityEnum.class)
                .setParameter(1, p_code);

        unityQuantity = (UnityQuantityEnum) query.getSingleResult();

        return unityQuantity;
    }//Ok

    /**
     * This method return a list of unity quantity
     *
     * @return List of UnityQuantityEnum
     */
    public List<UnityQuantityEnum> getUnityQuantityList() {
        List<UnityQuantityEnum> unitiesQuantity;
        unitiesQuantity = new ArrayList<>();

        Query query = con.getEM().createNativeQuery("select * from UNITESDEQUANTITE", UnityQuantityEnum.class);
        unitiesQuantity.addAll((List<UnityQuantityEnum>) query.getResultList());

        return unitiesQuantity;
    }//Ok

}
