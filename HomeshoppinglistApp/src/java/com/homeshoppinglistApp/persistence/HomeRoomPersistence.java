package com.homeshoppinglistApp.persistence;

import com.homeshoppinglistApp.business.Home;
import com.homeshoppinglistApp.business.HomeRoom;
import com.homeshoppinglistApp.business.RoomEnum;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.Query;

/**
 * This class allow to manage home room data from the data base
 *
 * @author BorisK <klettboris@gmail.com>
 */
@Stateless
public class HomeRoomPersistence implements Serializable {

    @Inject
    private Connexion con;
    private static final long serialVersionUID = 1L;

    /**
     * This is the constructor
     */
    public HomeRoomPersistence() {
    }

    /**
     * This method return a home room by its id
     *
     * @param p_id
     * @return
     */
    public HomeRoom getHomeRoomById(Integer p_id) {
        HomeRoom homeRoom;
        homeRoom = (HomeRoom) con.getEM().find(HomeRoom.class, p_id);
        return homeRoom;
    }//Ok

    /**
     * This method allow to check if the home room exist
     *
     * @param p_homeRoom research the home room
     * @return boolean
     */
    public boolean isRoomHomeExist(HomeRoom p_homeRoom) {
        boolean isOk = false;
        HomeRoom homeRoom;
        Query query = con.getEM().createNativeQuery("select * from PIECESDUFOYER where foy_numero = ? and pcs_numero = ?", HomeRoom.class)
                .setParameter(1, p_homeRoom.getHome().getId())
                .setParameter(2, p_homeRoom.getRoom().getId());
        try {
            homeRoom = (HomeRoom) query.getSingleResult();
            if (homeRoom != null) {
                isOk = true;
            }
        } catch (Exception ex) {
            return isOk;
        }

        return isOk;
    }//Ok

    /**
     * This method allow to add a room to home
     *
     * @param p_homeRoom
     */
    public void addRoomHome(HomeRoom p_homeRoom) {
        con.getEM().persist(p_homeRoom);
        con.getEM().flush();
    }//Ok

    /**
     * This method allow to remove a room from a home
     *
     * @param p_homeRoom
     */
    public void deleteRoomHome(HomeRoom p_homeRoom) {
        HomeRoom homeRoom = this.getHomeRoomById(p_homeRoom.getId());

        con.getEM().remove(homeRoom);
    }//Ok

    /**
     * This method allow to get rooms from home
     *
     * @param p_home
     * @return List of rooms
     */
    public List<HomeRoom> getRoomsByHome(Home p_home) {
        List<HomeRoom> homeRooms = new ArrayList<>();

        Query query = con.getEM().createNativeQuery("select * from PIECESDUFOYER where foy_numero = ?", HomeRoom.class)
                .setParameter(1, p_home.getId());

        homeRooms.addAll((List<HomeRoom>) query.getResultList());

        return homeRooms;
    }//Ok

    /**
     * This method allow to HomeRoom by a given home and a given room
     *
     * @param p_home
     * @param p_room
     * @return
     */
    public HomeRoom getHRByHomeNRoom(Home p_home, RoomEnum p_room) {
        HomeRoom hr;
        try {
            Query query = con.getEM().createNativeQuery("select * from PIECESDUFOYER where foy_numero = ? and pcs_numero = ?", HomeRoom.class)
                    .setParameter(1, p_home.getId())
                    .setParameter(2, p_room.getId());

            hr = (HomeRoom) query.getSingleResult();
        } catch (Exception ex) {
            hr = null;
        }

        return hr;

    }//New

}
