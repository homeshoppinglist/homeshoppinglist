package com.homeshoppinglistApp.persistence;

import java.io.Serializable;
import javax.ejb.Stateful;
import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.PersistenceContext;
import javax.transaction.Transaction;

/**
 * This class is for the connexion
 *
 * @author BorisK <klettboris@gmail.com>
 */
@Stateful
public class Connexion implements Serializable {

    @PersistenceContext(unitName = "HSLPersistence")
    private EntityManager em;
    private static final long serialVersionUID = 1L;

    /**
     * This is the constructor of the class Connection
     */
    public Connexion() {
    }

    /**
     * This method get the entity manager to connect my app to my database
     *
     * @return an instance of EntityManager
     */
    public EntityManager getEM() {
        return this.em;
    }

    /**
     * This method close the connexion to my database
     */
    public void closeConnexion() {
        em.clear();
        em.close();
    }

}
