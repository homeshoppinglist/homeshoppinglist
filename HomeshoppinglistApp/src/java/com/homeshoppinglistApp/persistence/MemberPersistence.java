package com.homeshoppinglistApp.persistence;

import com.homeshoppinglistApp.business.Home;
import com.homeshoppinglistApp.business.Membre;
import com.homeshoppinglistApp.business.User;
import com.homeshoppinglistApp.business.UserStatusEnum;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.Query;

/**
 *
 * This class allow to manage member data from the data base
 *
 * @author BorisK <klettboris@gmail.com>
 */
@Stateless
public class MemberPersistence implements Serializable {

    @Inject
    private Connexion con;

    //Todo make a service class
    @Inject
    private UserStatusPersistence userStatusPersistence;

    private static final long serialVersionUID = 1L;

    /**
     * This is the constructor
     */
    public MemberPersistence() {
    }

    /**
     * This method allow to return a member by its id
     *
     * @param p_id
     * @return Member
     */
    public Membre getMemberById(Integer p_id) {
        Membre member;
        member = (Membre) con.getEM().find(Membre.class, p_id);
        return member;
    }//Ok

    /**
     * This method allow to return a member by its address and email
     *
     * @param p_user
     * @param p_home
     * @return member with the id -1 if there isn't any result otherwise return
     * the finded member
     */
    public Membre getCurrentMemberShipByUserNHome(User p_user, Home p_home) {
        Membre member;
        try {
            Query query = con.getEM().createNativeQuery("select * from membres where foy_numero = ? and util_numero=? and DATEFIN is null", Membre.class)
                    .setParameter(1, p_home.getId())
                    .setParameter(2, p_user.getId());

            member = (Membre) query.getSingleResult();
        } catch (Exception ex) {
            member = null;
        }

        return member;
    }//Ok

    /**
     * This method allow to return a member by its address and email
     *
     * @param p_user
     * @param p_home
     * @return member with the id -1 if there isn't any result otherwise return
     * the finded member
     */
    public Membre getMemberByUserNHome(User p_user, Home p_home) {
        Membre member;
        try {
            Query query = con.getEM().createNativeQuery("select * from membres where foy_numero = ? and util_numero=? ", Membre.class)
                    .setParameter(1, p_home.getId())
                    .setParameter(2, p_user.getId());

            member = (Membre) query.getSingleResult();
        } catch (Exception ex) {
            member = null;
        }

        return member;
    }//Ok

    /**
     * This method allow to return a member by its address and email
     *
     * @param p_user
     * @param p_home
     * @return List<Membre>
     */
    public List<Membre> getMembersListByUserNHome(User p_user, Home p_home) {
        List<Membre> members;
        try {
            Query query = con.getEM().createNativeQuery("select * from membres where foy_numero = ? and util_numero=? ", Membre.class)
                    .setParameter(1, p_home.getId())
                    .setParameter(2, p_user.getId());

            members = (List<Membre>) query.getResultList();
        } catch (Exception ex) {
            members = new ArrayList<>();
        }

        return members;
    }//Ok

    /**
     * This method allow to return a member by its address and email
     *
     * @param p_user
     * @param p_home
     * @return List<Membre>
     */
    public List<Membre> getOldMembersShipListByUserNHome(User p_user, Home p_home) {
        List<Membre> members;
        try {
            Query query = con.getEM().createNativeQuery("select * from membres where foy_numero = ? and util_numero=? and DATEFIN is not null", Membre.class)
                    .setParameter(1, p_home.getId())
                    .setParameter(2, p_user.getId());

            members = (List<Membre>) query.getResultList();
        } catch (Exception ex) {
            members = new ArrayList<>();
        }

        return members;
    }//Ok

    /**
     *
     * @param p_user
     * @return List<Membre>
     */
    public List<Membre> getCurrentMembersShipListByUser(User p_user) {
        List<Membre> members;
        try {
            Query query = con.getEM().createNativeQuery("select * from membres where util_numero=? and DATEFIN is null", Membre.class)
                    .setParameter(1, p_user.getId());

            members = (List<Membre>) query.getResultList();
        } catch (Exception ex) {
            members = new ArrayList<>();
        }

        return members;
    }//Ok

    /**
     * This method allow to add a member to a home
     *
     * @param p_user
     * @param p_home
     * @param p_userStatus
     * @return the new member user
     */
    public Membre addHomeMember(User p_user, Home p_home, UserStatusEnum p_userStatus) {

        Membre member = new Membre();
        member.setHome(p_home);
        member.setStatus(p_userStatus);
        member.setStartDate(new Date());
        member.setUser(p_user);

        con.getEM().persist(member);

        return this.getMemberByUserNHome(p_user, p_home);
    }//Ok

    /**
     * This member delete a member by providing the enddate
     *
     * @param p_member
     * @param p_endDate
     */
    public void deleteMember(Membre p_member, Date p_endDate) {

        Membre member = this.getMemberById(p_member.getId());
        member.setEndDate(p_endDate);
        con.getEM().flush();

    }//Ok

    /**
     * This method allow to update member
     *
     * @param p_member
     */
    public void updateMember(Membre p_member) {
        Membre member = this.getMemberById(p_member.getId());
        member.setEndDate(p_member.getEndDate());
        member.setStartDate(p_member.getStartDate());
        member.setStatus(p_member.getStatus());

        con.getEM().flush();
    }//Ok

    /**
     * This method return members by their home
     *
     * @param p_home
     * @return List of members from a home
     */
    public List<Membre> getMembersByHome(Home p_home) {
        List<Membre> members = new ArrayList<>();

        Query query = con.getEM().createNativeQuery("select * from membres where foy_numero = ?", Membre.class)
                .setParameter(1, p_home.getId());
        members.addAll((List<Membre>) query.getResultList());

        return members;
    }//Ok

    /**
     * This method return all members
     *
     * @return List of members
     */
    public List<Membre> getMembersList() {
        List<Membre> members = new ArrayList<>();

        Query query = con.getEM().createNativeQuery("select * from membres", Membre.class);
        members.addAll((List<Membre>) query.getResultList());

        return members;
    }//Ok
}
