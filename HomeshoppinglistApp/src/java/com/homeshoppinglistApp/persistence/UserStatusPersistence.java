package com.homeshoppinglistApp.persistence;

import com.homeshoppinglistApp.business.UserStatusEnum;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.Query;

/**
 *
 * This class allow to manage member data from the data base
 *
 * @author BorisK <klettboris@gmail.com>
 */
@Stateless
public class UserStatusPersistence implements Serializable {

    @Inject
    private Connexion con;
    private static final long serialVersionUID = 1L;

    /**
     * This is the constructor
     */
    public UserStatusPersistence() {
    }

    /**
     * This method allow to return a status by its code
     *
     * @param p_code
     * @return User status
     */
    public UserStatusEnum getStatusByCode(String p_code) {
        UserStatusEnum userStatus;

        Query query = con.getEM().createNativeQuery("select * from ROLES WHERE UPPER (CODE) = UPPER(?)", UserStatusEnum.class)
                .setParameter(1, p_code);

        userStatus = (UserStatusEnum) query.getSingleResult();

        return userStatus;
    }//Ok

    /**
     * This method allow to return all the status
     *
     * @return list of user status
     */
    public List<UserStatusEnum> getUserSatusList() {
        List<UserStatusEnum> userStatus;
        userStatus = new ArrayList<>();

        Query query = con.getEM().createNativeQuery("select * from ROLES", UserStatusEnum.class);
        userStatus.addAll((List<UserStatusEnum>) query.getResultList());

        return userStatus;
    }//Ok

    /**
     * This method allow to return a user status by its id
     *
     * @param p_id
     * @return user status
     */
    public UserStatusEnum getUserStatusById(Integer p_id) {
        UserStatusEnum userStatus;
        userStatus = (UserStatusEnum) con.getEM().find(UserStatusEnum.class, p_id);
        return userStatus;
    }//OK
}
